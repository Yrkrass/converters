<?php

use Illuminate\Database\Seeder;
use Database\Seeds\Kernel\ConvertersTableSeeder;
use Database\Seeds\Kernel\PagesTableSeeder;
use Database\Seeds\Kernel\UsersTableSeeder as KernelUsersTableSeeder;
use Database\Seeds\Dummy\UsersTableSeeder as DummyUsersTableSeeder;
use Database\Seeds\Kernel\PageCategoriesTableSeeder as KernelPageCategoriesTableSeeder;
use Database\Seeds\Kernel\PageCategoriesAndGroupsTableSeeder;
use Database\Seeds\Dummy\PageCategoriesTableSeeder as DummyPageCategoriesTableSeeder;


class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->kernelSeed();
        $this->dummySeed();
    }

    private function kernelSeed()
    {
        $kernelSeeders = [
            ConvertersTableSeeder::class,
            KernelUsersTableSeeder::class,
//            KernelPageCategoriesTableSeeder::class,
            PageCategoriesAndGroupsTableSeeder::class,
            PagesTableSeeder::class,
        ];

        foreach ($kernelSeeders as $seeder) {
            $this->call($seeder);
        }
    }

    private function dummySeed()
    {
        if (config('app.debug')) {
            $dummySeeders = [
                DummyUsersTableSeeder::class,
                //DummyPageCategoriesTableSeeder::class,
            ];

            foreach ($dummySeeders as $seeder) {
                $this->call($seeder);
            }
        }
    }
}
