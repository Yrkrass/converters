<?php

namespace Database\Seeds\Kernel;

use App\Models\PageGroup;
use App\Models\PageCategory;
use Illuminate\Database\Seeder;

class PageCategoriesAndGroupsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'Common Converters' => [
                'Length',
                'Volume',
                'Area',
                'Energy',
                'Force',
                'Speed',
                'Fuel Consumption',
                'Data Storage',
                'Currency',
                'Weight and Mass',
                'Temperature',
                'Pressure',
                'Power',
                'Time',
                'Angle',
                'Numbers',
                'Volume - Dry',
                'Case',
            ],
            'Engineering Converters' => [
                'Volume',
                'Temperature',
                'Area',
                'Pressure',
                'Energy',
                'Power',
                'Force',
                'Time',
                'Speed',
                'Angle',
                'Fuel',
                'Consumption',
                'Numbers',
                'Data Storage',
                'Volume - Dry',
                'Currency',
                'Velocity - Angular',
                'Acceleration',
                'Acceleration - Angular',
                'Density',
                'Specific Volume',
                'Moment of Inertia',
                'Moment of ForceTorque',
            ],
            'Heat Converters' => [
                'Fuel Efficiency - Mass',
                'Fuel Efficiency - Volume',
                'Temperature Interval',
                'Thermal Expansion',
                'Thermal Resistance',
                'Thermal Conductivity',
                'Specific Heat Capacity',
                'Heat Density',
                'Heat Flux Density',
                'Heat Transfer Coefficient',
            ],
            'Fluids Converters' => [
                'Flow',
                'Flow - Mass',
                'Flow - Molar',
                'Mass Flux Density',
                'Concentration - Molar',
                'Concentration - Solution',
                'Viscosity - Dynamic',
                'Viscosity - Kinematic',
                'Surface Tension',
                'Permeability',
            ],
            'Light Converters' => [
                'Luminance',
                'Luminous Intensity',
                'Illumination',
                'Digital Image Resolution',
                'Frequency Wavelength',
            ],
            'Electricity Converters' => [
                'Charge',
                'Linear Charge Density',
                'Surface Charge Density',
                'Volume Charge Density',
                'Current',
                'Linear Current Density',
                'Surface Current Density',
                'Electric Field Strength',
                'Electric Potential',
                'Electric Resistance',
                'Electric Resistivity',
                'Electric Conductance',
                'Electric Conductivity',
                'Electrostatic Capacitance',
                'Inductance',
            ],
            'Magnetism Converters' => [
                'Magnetomotive Force',
                'Magnetic Field Strength',
                'Magnetic Flux',
            ],
            'Radiology Converters' => [
                'Radiation',
                'Radiation-Activity',
                'Radiation-Exposure',
                'Radiation-Absorbed Dose',
            ],
        ];

        foreach ($data as $categoryName => $groupNames) {
            $category = PageCategory::firstOrCreate(['name' => $categoryName, 'slug' => str_slug($categoryName)]);

            foreach ($groupNames as $groupName) {
                $group = PageGroup::firstOrCreate(['name' => $groupName, 'slug' => str_slug($groupName)]);
                $group->categories()->attach($category->id);
            }
        }
    }
}
