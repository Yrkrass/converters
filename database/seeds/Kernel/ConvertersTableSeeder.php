<?php

namespace Database\Seeds\Kernel;

use App\Models\Converter;
use Illuminate\Database\Seeder;

class ConvertersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'cm_dcm',
                'from' => 'Centimeter',
                'to' => 'Decimeter',
                'from_callback' => 'from * 10',
                'to_callback' => 'to / 10',
                'from_type' => 'number',
                'to_type' => 'number',
            ],
            [
                'name' => 'mph_kph',
                'from' => 'mile/hour',
                'to' => 'kilometer/hour',
                'from_callback' => 'from * 1.609344',
                'to_callback' => 'to / 1.609344',
                'from_type' => 'number',
                'to_type' => 'number',
            ],
            [
                'name' => 'cm_mm',
                'from' => 'Centimeter',
                'to' => 'Millimeter',
                'from_callback' => 'from / 10',
                'to_callback' => 'to * 10',
                'from_type' => 'number',
                'to_type' => 'number',
            ],
            [
                'name' => 'str_md5',
                'from' => 'String',
                'to' => 'md5 hash',
                'from_callback' => 'md5',
                'type' => 'one_way',
            ],
            [
                'name' => 'dec_bin',
                'from' => 'Dec',
                'to' => 'Bin',
                'from_callback' => 'decbin',
                'to_callback' => 'bindec',
                'type' => 'two_ways',
                'from_type' => 'number',
                'to_type' => 'number',
            ],
        ];

        foreach ($data as $converterData) {
            Converter::firstOrCreate($converterData);
        }
    }
}

