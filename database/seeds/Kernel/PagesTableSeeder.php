<?php

namespace Database\Seeds\Kernel;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    public function run()
    {
        $pages = [
            '/' => [
                'title' => 'Home',
                'seo_title' => 'Home',
            ],
            'archive' => [
                'title' => 'Archive',
                'seo_title' => 'Seo Archive',
                'seo_description' => 'Seo Archive desc',
                'seo_keywords' => 'Seo Archive keywords',
                'content' => '
                <h4>Test title</h4><hr>
                <p>
                Object-oriented programming (OOP) is a <a href="#">programming paradigm</a> based on the concept of "objects", which may contain data, 
                in the form of fields, often known as attributes; and code, in the form of procedures, often known as methods.
                A feature of objects is that an object\'s procedures can access and often modify the data 
                fields of the object with which they are associated (objects have a notion of "this" or "self").
                </p>
                <p>@conv(mph_kph)</p>
                <ul>
                    <li>joule [J]</li>
                    <li>1 <a href="#">kilojoule</a> [kJ] = 1000 joule [J]</li>
                    <li>kilojoule to joule,   joule to kilojoule</li>
                    <li>1 kilowatt-hour [kW*h] = 3600000 joule [J]</li>
                    <li>kilowatt-hour to joule,   joule to kilowatt-hour</li>
                    <li>1 watt-hour [W*h] = 3600 joule [J]</li>
                    <li>watt-hour to joule,   joule to watt-hour</li>
                    <li>1 calorie (nutritional) = 4186.8 joule [J]</li>
                    <li>calorie (nutritional) to joule,   joule to calorie (nutritional)</li>
                </ul>
                <table>
                    <tr>
                        <td>1.</td>
                        <td>Body & wrapper close</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Main Navigation or menu</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Responsible for site pre header</td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Responsible for all site scripts</td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>aper is using partials folder to inject</td>
                    </tr>
                </table>
                <p>
                In OOP, computer programs are designed by making them out of objects that interact with one another.[1][2] There 
                is significant diversity of OOP languages, but the most popular ones are class-based, meaning that objects are 
                instances of classes, which typically also determine their type.
                </p>
                <p>@conv(cm_dcm)</p>
                <p>@conv(str_md5)</p>
                <p>@conv(dec_bin)</p>',
                'page_group_id' => 1,
            ],
            'page-category' => [
                'title' => 'Page Category',
                'seo_title' => 'Page Category',
                'seo_description' => 'Seo Category desc',
                'seo_keywords' => 'Seo Category keywords',
            ],
            'page-group' => [
                'title' => 'Page Group',
                'seo_title' => 'Page Group',
                'seo_description' => 'Seo Group desc',
                'seo_keywords' => 'Seo Group keywords',
            ],
        ];

        foreach ($pages as $slug => $data) {
            $page = Page::firstOrNew([
                'slug' => $slug,
            ]);

            if (!$page->exists) {
                $page->fill($data)->save();
            }
        }
    }
}
