<?php

namespace Database\Seeds\Kernel;

use App\Models\PageCategory;
use Illuminate\Database\Seeder;

class PageCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $categories = [
            'Common Converters',
            'Engineering Converters',
            'Heat Converters',
            'Fluids Converters',
            'Light Converters',
            'Electricity Converters',
            'Magnetism Converters',
            'Radiology Converters',
        ];

        foreach($categories as $i => $name) {
            PageCategory::firstOrCreate(['id' => ++$i, 'name' => $name, 'slug' => str_slug($name)]);
        }
    }
}
