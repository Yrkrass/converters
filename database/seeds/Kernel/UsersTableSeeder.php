<?php

namespace Database\Seeds\Kernel;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = User::firstOrNew([
            'email' => 'admin@myconverter.info',
        ]);

        if (!$user->exists) {
            $user->fill([
                'name' => 'Digital Services',
                'is_active' => 1,
                'password' => bcrypt('pass123'),
                'remember_token' => str_random(10)
            ])->save();
        }
    }
}
