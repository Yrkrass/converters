<?php

namespace Database\Seeds\Dummy;

use App\Models\PageCategory;
use Illuminate\Database\Seeder;

class PageCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        factory(PageCategory::class)->times(6)->create();
    }
}
