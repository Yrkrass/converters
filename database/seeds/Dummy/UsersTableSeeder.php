<?php

namespace Database\Seeds\Dummy;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        factory(User::class)->times(20)->create();
    }
}
