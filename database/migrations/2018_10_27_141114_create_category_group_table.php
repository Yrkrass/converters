<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_group', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('page_category_id');
            $table->unsignedInteger('page_group_id');
        });

        Schema::table('category_group', function(Blueprint $table) {
            $table->unique(['page_category_id', 'page_group_id']);

            $table->foreign('page_category_id')->references('id')->on('page_categories')->onDelete('cascade');
            $table->foreign('page_group_id')->references('id')->on('page_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_group');
    }
}
