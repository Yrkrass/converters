<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvertersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('converters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->unique();
            $table->string('from');
            $table->string('to');

            $table->enum('from_type', ['text', 'number'])->default('text');
            $table->enum('to_type', ['text', 'number'])->default('text');

            $table->string('from_callback');
            $table->string('to_callback')->nullable();

            $table->enum('type', ['multiplier', 'one_way', 'two_ways'])->default('multiplier');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('converters');
    }
}
