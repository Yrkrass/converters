<?php
Route::namespace('Frontend')->group(function () {
    Route::get('/', 'PageController@index')->name('index');

    Route::group(['middleware' => ['web'], 'as' => 'page.'], function () {
        Route::get('/category/{categorySlug}', 'PageController@pageCategory')->name('category');
        Route::get('/group/{groupSlug}', 'PageController@pageGroup')->name('group');
        Route::get('/sitemap', 'PageController@sitemap')->name('sitemap');
        Route::get('/contacts', 'PageController@contacts')->name('contacts');
        Route::post('/contacts', 'ContactsController@sendEmail')->name('contacts.send');
        Route::get('/about-us', 'PageController@aboutUs')->name('about-us');
        Route::get('/privacy-policy', 'PageController@privacy')->name('privacy');
    });

    Route::get('/{pageSlug}', 'PageController@page')->name('page');

    Route::post('/converter', 'ConverterController@converter')->name('converter');
    Route::post('/convert-one-way/{converterName}', 'ConverterController@convertOneWay')->name('convert.one_way');
    Route::post('/convert-two-ways/{converterName}', 'ConverterController@convertTwoWays')->name('convert.two_ways');
});
