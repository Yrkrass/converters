<?php
Route::group(['prefix' => 'admin'], function () {
    Route::namespace('Backend')->group(function () {
        Auth::routes();

        Route::redirect('/register', '/admin/login', 301);
        Route::redirect('/password/reset', '/admin/login', 301);

        Route::group(['middleware' => ['auth', 'web'], 'as' => 'admin.'], function () {
            Route::get('/', 'DashboardController@index')->name('index');

            Route::get('/generate/sitemap', 'SitemapController@generate')->name('generate.sitemap');

            Route::get('/generators/table', 'GeneratorController@table')->name('generators.table');
            Route::get('/generators/page', 'GeneratorController@page')->name('generators.page');
            Route::post('/generators/page/store', 'GeneratorController@pageStore')->name('generators.page.store');

            Route::get('converters/mass-create', 'ConverterController@massCreate')->name('converters.mass-create');
            Route::post('converters/mass-store', 'ConverterController@massStore')->name('converters.mass-store');

            Route::resource('users', 'UserController');
            Route::resource('pages', 'PageController');
            Route::resource('page-groups', 'PageGroupController');
            Route::resource('page-categories', 'PageCategoryController');
            Route::resource('converters', 'ConverterController');

        });
    });
});
