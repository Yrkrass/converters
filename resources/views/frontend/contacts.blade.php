@php
    /* @var $page \App\Models\Page */
@endphp

@extends('frontend.layouts.master')

@section('seo-title', $page->seo_title ?? 'Contacts' . ' | ' . config('app.name'))
@section('description', $page->seo_description . ' | ' . config('app.name'))
@section('keywords', $page->seo_keywords)
@section('page-title', $page->title)

@section('search-section')
    <div class="col-12">
        <h1>{{ $page->title ?? 'Contacts' }}</h1>
    </div>
@endsection

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                <li class="active">{{ $page->title ?? 'Contacts' }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-xl-12">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-md-8 col-12">
                <p>
                    If you have any questions or suggestions on new tools to be developed, please feel free to contact us.
                </p>
                <p>
                    Your name and email address provided in the form will not be used for marketing campaign
                    purposes and will never be shared with third parties.
                </p>

                @if(session('message-success'))
                    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                        {!! session('message-success') !!}
                    </div>
                @endif

                <form action="{{ route('page.contacts.send') }}" method="post">

                    @csrf

                    <div class="row">

                        <div class="col-12 col-lg-6">
                            <label for="name" class="required">Name <span class="req">*</span></label>
                            <input class="form-control form-control-lg" type="text" name="name" id="name" value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <label class="validation-error-label">{{ $errors->first('name') }}</label>
                            @endif
                        </div>

                        <div class="col-12 col-lg-6">
                            <label for="email" class="required">Email Address <span class="req">*</span></label>
                            <input class="form-control form-control-lg" type="text" name="email" id="email" value="{{ old('email') }}">
                            @if($errors->has('email'))
                                <label class="validation-error-label">{{ $errors->first('email') }}</label>
                            @endif
                        </div>

                        <div class="col-12">
                            <label for="subject" class="required">Subject</label>
                            <input class="form-control form-control-lg" type="text" name="subject" id="subject" value="{{ old('subject') }}">
                            @if($errors->has('subject'))
                                <label class="validation-error-label">{{ $errors->first('subject') }}</label>
                            @endif
                        </div>

                    </div>

                    <label for="text" class="required">Message <span class="req">*</span></label>
                    <textarea name="text" id="text" class="form-control form-control-lg">{{ old('text') }}</textarea>
                    @if($errors->has('text'))
                        <label class="validation-error-label">{{ $errors->first('text') }}</label>
                    @endif

                    <div class="text-right p-t-20">
                        <button class="btn btn-lg btn-info"><i class="fa fa-envelope-o"></i> Send Message</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
