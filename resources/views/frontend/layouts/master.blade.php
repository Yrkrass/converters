<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129653917-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-129653917-1');
    </script>

    <meta name="msvalidate.01" content="5E57EA449AC962ECC35EDD12BD1FFE59" />

    <meta name="norton-safeweb-site-verification" content="uej1iw4sg-185uo57yx4kdq27s-ydvqfru0i9o-dwkxe2yinfj8da2zryrrggsmz9e7ivmp8vtfzwz6y9n-cis17ez7ppfi1bb9hme5sr53lax-syvt0-88s91zmt4xy" />

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

    <meta charset="UTF-8">

    <title>@yield('seo-title', config('app.name'))</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,700,900">
    <link rel="stylesheet" href="{{ url('css/app.css') }}"/>
    <link rel="stylesheet" href="{{ url('css/responsive.css') }}"/>
    @stack('styles-header')
    @stack('scripts-header')
</head>
<body class="{{ \Route::currentRouteName() }}">
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <a href="{{ route('index') }}" class="logo"><span>My</span>Converter<small>.info</small></a>
            </div>
            <div class="col-sm-12 col-md-8">
                <ul class="nav float-right">
                    <li><a href="/">Home</a></li>
                    {{--<li class="drop-down">
                        <a href="#">Drop down</a>
                        <div class="dd-content">
                            <span class="dd-header">Title</span>
                            <ul>
                                <li><a href="#">Freely usable photos & more</a></li>
                                <li><a href="#">Free travel guide</a></li>
                                <li><a href="#">Community coordination & documentation</a></li>
                            </ul>
                        </div>
                    </li>--}}
                    <li><a href="{{ route('page.about-us') }}">About us</a></li>
                    <li><a href="{{ route('page.contacts') }}">Contacts</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<main>
    <section class="search-section">
        <div class="container">
            <div class="row">
                @yield('search-section')
            </div>
        </div>
    </section>

    @yield('breadcrumbs-section')

    <section class="content">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container links">
        <div class="row">
            <div class="col-md-12 col-lg-3">
                <a href="/" class="logo"><span>My</span>Converter<small>.info</small></a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <h6>Popular converters</h6>
                <ul class="footer-links">
                    @foreach($popularPages as $page)
                        <li><a href="{{ route('page', ['pageSlug'=> $page->slug]) }}">{{ $page->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <h6>Popular groups</h6>
                <ul class="footer-links">
                    @foreach($popularGroups as $group)
                    <li><a href="{{ route('page.group', ['groupSlug'=> $group->slug]) }}">{{ $group->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <h6>Useful links</h6>
                <ul class="footer-links">
                    <li><a href="{{ route('page.about-us') }}">About us</a></li>
                    <li><a href="{{ route('page.contacts') }}">Contacts</a></li>
                    <li><a href="{{ route('page.privacy') }}">Privacy policy</a></li>
                    <li><a href="{{ route('page.sitemap') }}">Sitemap</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-12 copyrights">
        <div class="container">
            <div>
                <p>&copy; {{ date('Y') }} All Rights reserved</p>
                <a href="//www.dmca.com/Protection/Status.aspx?ID=46ae858a-c2b5-4064-a82e-5002ad350d28" target="_blank" title="DMCA.com Protection Status" class="dmca-badge">
                    <img src ="https://images.dmca.com/Badges/dmca-badge-w100-5x1-08.png?ID=46ae858a-c2b5-4064-a82e-5002ad350d28"  alt="DMCA.com Protection Status" />
                </a>
                <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"></script>
            </div>
        </div>
    </div>
</footer>

@stack('styles-footer')

{{--<script>
    var loadDeferredStyles = function () {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement);
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (raf) raf(function () {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>--}}


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{ url('js/lib/cookie.js') }}"></script>
<script defer src="{{ url('js/converters.js') }}"></script>
<script defer src="{{ url('js/interface.js') }}"></script>

<script type="text/javascript">
    (function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        window.globals = {
            "converters": [],
            "routes": {
                "converter": "{{ route('converter') }}"
            }
        };
    })(jQuery);
</script>
@stack('scripts-footer')
</body>
</html>

