@extends('frontend.layouts.master')

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                @if($page->group)
                <li><a href="{{ route('page.group', ['groupSlug' => $page->group->slug]) }}">{{ $page->group->name }}</a></li>
                @endif
                <li class="active">{{ $page->title }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('search-section')
    <div class="col-12">
        <h1>@yield('page-title')</h1>
    </div>
@endsection

@section('content')
    <aside class="col-sm-12 col-md-4 col-lg-3 main-sidebar">
        <ul class="sidebar-menu">
            @foreach($pageCategories as $category)
            <li data-name="{{ $category->slug }}" class="{{ $category->groups->count() ? 'treeview closed' : '' }}">
                <a href="#">
                    <i class="fa fa-circle-o" style="color: {{ rand_color($category->id) }}"></i><span class="badge pull-right">{{ $category->groups->count() }}</span> <span>{{ $category->name }}</span>
                </a>
                @if($category->groups->count())
                <ul class="treeview-menu">
                    @foreach($category->groups as $group)
                    <li><a {{ $page->group->slug == $group->slug ? 'class=bold' : '' }} href="{{ route('page.group', ['groupSlug' => $group->slug]) }}">{{ $group->name }}</a></li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
    </aside>

    <article class="col-sm-12 col-md-8 col-lg-9">
        @yield('page-content')
    </article>
@endsection