@php
    /* @var $converter \App\Models\Converter */
@endphp

<form action="#" data-type="multiplier">
    <div class="row">
        <div class="col-md-4 col-lg-5">
            <div class="form-group">
                <input name="from" type="{{ $converter->from_type }}" value="" autocomplete="off" class="form-control form-control-lg" placeholder="{{ $converter->from }}"/>
            </div>
        </div>
        <div class="col-md-4 col-lg-5">
            <div class="form-group">
                <input name="to" type="{{ $converter->to_type }}" value="" autocomplete="off" class="form-control form-control-lg" placeholder="{{ $converter->to }}"/>
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="form-group">
                <button type="reset" class="btn btn-info btn-lg btn-block">Reset</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h5 class="result"></h5>
        </div>
    </div>

    {!! $converter->getMultiplierJs() !!}
</form>