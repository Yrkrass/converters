@php
    /* @var $converter \App\Models\Converter */
@endphp

<form action="{{ route('convert.one_way', ['converterName' => $converter->name]) }}" data-type="one_way">
    <div class="row">
        <div class="col-md-4 col-lg-8">
            <div class="form-group">
                <input name="from" type="{{ $converter->from_type }}" value="" autocomplete="off" class="form-control form-control-lg" placeholder="From {{ $converter->from }} to {{ $converter->to }}"/>
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-lg btn-block">Convert</button>
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="form-group">
                <button type="reset" class="btn btn-info btn-lg btn-block">Reset</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h5 class="result"></h5>
        </div>
    </div>
</form>