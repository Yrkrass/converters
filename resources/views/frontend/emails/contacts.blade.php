<h1>Mail from {{ $name }}</h1>
<div><b>Subject: {{ $subject }}, From: {{ $email }}</b></div>
<p>{{ $text }}</p>