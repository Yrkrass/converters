@extends('frontend.layouts.master')

@section('seo-title', $page->seo_title . ' | ' . config('app.name'))
@section('keywords', $page->seo_keywords)
@section('description', $page->seo_description)

@section('search-section')
    <div class="justify-content-center">
        <div class="col-lg-8 col-md-10 col-12">
            <div class="home-search">
                <h1>MyConverter<small>.info</small></h1>
                <p class="lead">is a website which provide free online tools for easy and very effective conversion</p>
                <form action="#" class="search-form">
                    <input type="text" class="search-field" placeholder="Search Converter ... ">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    @foreach($pageCategories as $category)
    <div class="col-xl-4 col-md-6">
        <div class="topics-wrapper border-style">
            <h3>
                <a href="{{ route('page.category', ['categorySlug' => $category->slug]) }}"><span class="fa fa-circle-o" style="color: {{ rand_color($category->id) }}"></span>{{ $category->name }}</a>
            </h3>
            @if($category->groups->count())
            <ul class="topics-list">
                @for($i = 0; $i < count($category->groups) && $i < 5; $i++)
                @php $categoryGroup = $category->groups[$i] @endphp
                <li><a href="{{ route('page.group', ['groupSlug' => $categoryGroup->slug]) }}">{{ $categoryGroup->name }}</a></li>
                @endfor
            </ul>
            @endif
            <ul class="topics-meta">
                <li>{{ $category->groups->count() }} total</li>
                <li><a href="{{ route('page.category', ['categorySlug' => $category->slug]) }}">view all</a></li>
            </ul>
        </div>
    </div>
    @endforeach

@endsection
