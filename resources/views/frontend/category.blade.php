@php
    /* @var $pageCategory \App\Models\PageCategory */
    /* @var $page \App\Models\Page */
@endphp

@extends('frontend.layouts.master')

@section('seo-title', $pageCategory->name . ' at ' . config('app.name') . ' | Free Online Tool')
@section('description', $pageCategory->name . ' at ' . config('app.name') . '. Use our online calculator for ' . $pageCategory->name . ' conversions.')
@section('keywords', $page->seo_keywords)
@section('page-title', $page->title)

@section('search-section')
    <div class="col-12">
        <h1>{{ $pageCategory->name }}</h1>
    </div>
@endsection

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                <li class="active">{{ $page->title }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <article class="col-xl-8 mx-lg-auto p-t-b-80">
        <ul class="icon-list col-2-list">
            @foreach($pageCategory->groups as $item)
                <li><span class="icon fa fa-circle-o" style="color: {{ rand_color($item->id) }}"></span><a href="{{ route('page.group', ['groupSlug' => $item->slug]) }}">{{ $item->name }}</a></li>
            @endforeach
        </ul>
    </article>
@endsection
