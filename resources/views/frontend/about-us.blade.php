@php
    /* @var $page \App\Models\Page */
@endphp

@extends('frontend.layouts.master')

@section('seo-title', $page->seo_title ?? 'About us' . ' | ' . config('app.name'))
@section('description', $page->seo_description . ' | ' . config('app.name'))
@section('keywords', $page->seo_keywords)
@section('page-title', $page->title)

@section('search-section')
    <div class="col-12">
        <h1>{{ $page->title ?? 'About us' }}</h1>
    </div>
@endsection

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                <li class="active">{{ $page->title ?? 'About us' }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <article class="col-xl-12 mx-lg-auto p-t-b-80">
        <div class="row">
            <div class="col-md-7"><h2>Thank you for visiting MyConverter.info</h2>
                <p>
                    We are a team of IT professionals working to create free content and various online tools.
                    Our project was established in 2018 with the objective to have the most useful online tools that
                    could help everybody in the routine of life gathered in one place making them as beneficial and easy to use as we possibly can.
                </p>
                <p>
                    Our project was created out of pure enthusiasm of our team. All works have been created exclusively in a home setting with
                    no investments made whatsoever. Nevertheless, the project has been developing: we keep expanding it and improving its products.
                </p>
            </div>
            <div class="col-md-5 text-center">
                <div class="p-t-40">
                    <img src="{{ url('img/icon-about-us.png') }}" alt="" />
                </div>
            </div>
        </div>
    </article>
@endsection
