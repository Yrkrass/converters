@extends('frontend.layouts.master')

@section('seo-title', ($page->seo_title ?? 'Sitemap') . ' | ' . config('app.name'))
@section('keywords', $page->seo_keywords)
@section('description', $page->seo_description)
@section('page-title', $page->title)

@section('search-section')
    <div class="col-12">
        <h1>{{ $page->title ?? 'Sitemap' }}</h1>
    </div>
@endsection

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                <li class="active">{{ $page->title ?? 'Sitemap' }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    @php
        $cols = 3;
        $count = round(count($pageCategories) / $cols)
    @endphp
    @for($i = 0; $i < $cols; $i++)
    <div class="col-xl-4 col-md-6">
        <div class="topics-wrapper border-style">
            @php $offset = intval($i * $count); @endphp
            @for($j = $offset; $j < ($count + $offset) && $j < count($pageCategories); $j++)
                @php $category = $pageCategories[$j]; @endphp
                <h3>
                    <a href="{{ route('page.category', ['categorySlug' => $category->slug]) }}"><span class="fa fa-circle-o" style="color: {{ rand_color($category->id) }}"></span>{{ $category->name }}</a>
                </h3>
                @if($category->groups->count())
                    <ul class="topics-list">
                        @foreach($category->groups as $categoryGroup)
                            <li>
                                <a href="{{ route('page.group', ['groupSlug' => $categoryGroup->slug]) }}">{{ $categoryGroup->name }}</a>
                                <ul>
                                    @foreach($categoryGroup->pages as $pageItem)
                                        @if($pageItem->is_active)
                                        <li>
                                            <a href="{{ route('page', ['pageSlug' => $pageItem->slug]) }}">{{ $pageItem->title }}</a>
                                        </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <ul class="topics-meta">
                    <li>{{ $category->groups->count() }} total</li>
                </ul>
            @endfor
        </div>
    </div>
    @endfor
@endsection