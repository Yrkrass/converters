@php
    /* @var $page \App\Models\Page */
@endphp

@extends('frontend.layouts.page')

@section('seo-title', $page->seo_title . ' | ' . config('app.name'))
@section('keywords', $page->seo_keywords)
@section('description', $page->seo_description)
@section('page-title', $page->title)

@section('page-content')
    {!! $page->showContent() !!}
@endsection
