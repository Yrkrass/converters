@php
    /* @var $pageGroup \App\Models\PageGroup */
    /* @var $page \App\Models\Page */
@endphp

@extends('frontend.layouts.master')

@section('seo-title', $pageGroup->name . ' Converters at ' . config('app.name') . ' | Free Online Tool')
@section('description', $pageGroup->name . ' converters at ' . config('app.name') . '. Use our online calculator for ' . $pageGroup->name . ' conversions.')
@section('keywords', $page->seo_keywords)
@section('page-title', $page->title)

@section('search-section')
    <div class="col-12">
        <h1>{{ $pageGroup->name }}</h1>
    </div>
@endsection

@section('breadcrumbs-section')
    <div class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="{{ route('index') }}">Home</a></li>
                <li class="active">{{ $page->title }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <article class="col-xl-8 mx-lg-auto p-t-b-80">
        <ul class="icon-list">
            @foreach($pageGroup->pages as $item)
                @if($item->is_active)
                <li><span class="icon fa fa-circle-o" style="color: {{ rand_color($item->id) }}"></span><a href="{{ route('page', ['pageSlug' => $item->slug]) }}">{{ $item->title }}</a></li>
                @endif
            @endforeach
        </ul>
    </article>
@endsection
