<!DOCTYPE html>
<html>
<head>
    @section('head')
        <title>@yield('title', config('app.name', ''))</title>

        <meta charset="UTF-8">
        <meta name="description" content="@yield('meta-description', '')"/>
        <meta name="keywords" content="@yield('meta-keywords', '')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="referrer" content="origin-when-crossorigin" id="meta_referrer"/>

        <link rel="canonical" href="@yield('canonical', url()->current())"/>

        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

        <link href="{{ asset('cms/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/colors.css') }}" rel="stylesheet" type="text/css">

        @stack('styles-header')
        @stack('scripts-header')
    @show
</head>

<body class="login-container bg-slate-800">

@section('body')
    <div class="page-container">
        <div class="page-content">
            <div class="content-wrapper">
                <div class="content">

                    @yield('content')

                    <div class="footer text-muted text-center">
                        &copy; {{ date('Y') }}. All Rights Reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stack('styles-footer')
    @stack('scripts-footer')
@show
</body>
</html>
