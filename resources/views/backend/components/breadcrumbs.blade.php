<ul class="breadcrumb">
    <li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
    @isset($elements)
    @foreach($elements as $route => $name)
        <li><a href="{{ route($route) }}">{{ $name }}</a></li>
    @endforeach
    @endif
    @isset($current)<li>{{ $current }}</li>@endisset
</ul>