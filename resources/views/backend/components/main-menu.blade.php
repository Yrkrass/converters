@foreach($items as $item)
    @php
        $route = isset($item['route']) ? route($item['route']) : '#';
    @endphp
    <li {{ $route != '#' ? highlight_menu_item($item['route']) : '' }}>
        <a href="{{ $route }}" {{ isset($item['children']) ? 'class=has-ul' : '' }}>
            <i class="{{ $item['icon'] }}"></i>
            <span>{{ $item['title'] }}
                @isset($item['count'])
                <span class="label bg-blue-400">{{ $item['count'] }}</span>
                @endisset
            </span>
        </a>
        @isset($item['children'])
        <ul>
            @foreach($item['children'] as $child)
                <li {{ isset($child['route']) && $child['route'] != '#' ? highlight_menu_item($child['route']) : '' }}>
                    <a href="{{ route($child['route']) }}">
                        {{ $child['title'] }}
                        @isset($child['count'])
                            <span class="label bg-blue-400">{{ $child['count'] }}</span>
                        @endisset
                    </a>
                </li>
            @endforeach
        </ul>
        @endisset
    </li>
@endforeach