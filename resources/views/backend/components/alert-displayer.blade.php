@if(session('message-error'))
    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-component">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        {!! session('message-error') !!}
    </div>
@endif

@if(session('message-success'))
    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
        <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">Well done!</span> {!! session('message-success') !!}
    </div>
@endif

@if(session('message-info'))
    <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        {!! session('message-info') !!}
    </div>
@endif