<a href="{{ route($route) }}" class="btn btn-primary">
<i class="{{ isset($icon) ? $icon : 'icon-plus3' }} position-left"></i>
@isset($title) {{ $title }} @else Create new {{ $name }} @endisset
</a>