@php
    /**
        $name
        $title
        $is_required
        $errors
        $model
        $type
    */
@endphp


<div class="form-group">
    <label class="control-label col-lg-2">{{ $title ?? ucwords(str_replace('_', ' ', $name)) }}@isset($is_required) <span class="text-danger">*</span>@endif</label>
    <div class="col-lg-10">
        <input type="{{ $type ?? 'text' }}" name="{{ $name }}" placeholder="{{ ucwords(str_replace('_', ' ', $name)) }}" value="{{ (old($name) ?: (isset($model) ? $model->$name : '')) }}" class="form-control">

        @if($errors->has($name))
            <label class="validation-error-label">{{ $errors->first($name) }}</label>
        @endif
    </div>
</div>
