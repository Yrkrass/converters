@php
    /**
        $name
        $title
        $is_required
        $errors
        $items
        $selected_value
        $item_name
        $allow_null
    */
@endphp

<div class="form-group">
    <label class="control-label col-lg-2">{{ $title ?? ucwords(str_replace('_', ' ', $name)) }}@isset($is_required) <span class="text-danger">*</span>@endif</label>
    <div class="col-lg-10">
        <select name="{{ $name }}" class="select">
            @if(isset($allow_null) && $allow_null)
            <option value="0">--</option>
            @endif
            @foreach($items as $item)
                <option {{ isset($selected_value) && $item->id == $selected_value ? 'selected=selected' : '' }} value="{{ $item->id }}">{{ isset($item_name) ? $item->$item_name : $item }}</option>
            @endforeach
        </select>

        @if($errors->has($name))
            <label class="validation-error-label">{{ $errors->first($name) }}</label>
        @endif
    </div>
</div>