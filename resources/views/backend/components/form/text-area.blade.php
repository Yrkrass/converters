@php
    /**
        $name
        $title
        $is_required
        $errors
        $model
    */
@endphp

<div class="form-group">
    <label class="control-label col-lg-2">{{ $title ?? ucwords(str_replace('_', ' ', $name)) }}@isset($is_required) <span class="text-danger">*</span>@endif</label>
    <div class="col-lg-10">
        <textarea class="form-control" name="{{ $name }}" rows="2">{{ old($name) ?: (isset($model) ? $model->$name : '') }}</textarea>

        @if($errors->has($name))
            <label class="validation-error-label">{{ $errors->first($name) }}</label>
        @endif
    </div>
</div>
