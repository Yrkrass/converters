<div class="form-group">
    <label class="control-label col-lg-2">{{ $title ?? ucwords(str_replace('_', ' ', $name)) }}</label>
    <div class="col-lg-10">
        <div class="checkbox checkbox-switchery">
            <label>
                <input type="hidden" name="{{ $name }}" value="0"/>
                <input type="checkbox" class="switchery" {{ (isset($model) && $model->$name) || (!isset($model->$name) && $is_default_checked) ? 'checked=checked' : '' }} value="1" name="{{ $name }}">
            </label>
        </div>
    </div>
</div>