<div class="form-group">
    <label class="control-label col-lg-2">Created at</label>
    <div class="col-lg-10">
        <div class="input-group">
            <i class="icon-calendar2 position-left"></i> {{ $model->created_at }}
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-lg-2">Updated at</label>
    <div class="col-lg-10">
        <div class="input-group">
            <i class="icon-calendar2 position-left"></i> {{ $model->updated_at }}
        </div>
    </div>
</div>