@extends('backend.layouts.master')

@section('title', 'Page generate')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page</span> - Generate</h4>
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'page', 'route' => 'admin.pages.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page generate', 'elements' => [
        'admin.pages.index' => 'Pages'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Generate Page with Converter'])

            <div class="panel-body">

                <form action="{{ route('admin.generators.page.store') }}" method="post" class="form-horizontal">

                    @csrf

                    <fieldset class="content-group">
                        <legend class="text-bold">General</legend>

                        @include('backend.components.form.text-field', [
                           'name' => 'from',
                           'is_required' => true,
                           'errors' => $errors,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'to',
                            'is_required' => true,
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'short_from',
                            'is_required' => true,
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'short_to',
                            'is_required' => true,
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'multiplier',
                            'is_required' => true,
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'example_num',
                            'is_required' => true,
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.checkbox', [
                            'title' => 'Is from longer',
                            'name' => 'is_from_greater',
                            'is_default_checked' => true
                        ])
                    </fieldset>

                    <fieldset class="content-group">
                        <legend class="text-bold">Page data</legend>

                        @include('backend.components.form.select', [
                            'name' => 'page_group_id',
                            'allow_null' => false,
                            'errors' => $errors,
                            'items' => $groups,
                            'item_name' => 'name',
                            'selected_value' => old('page_group_id') ?? null
                        ])

                        @include('backend.components.form.text-area', [
                            'name' => 'chart_table',
                            'errors' => $errors,
                        ])

                        @include('backend.components.form.checkbox', ['name' => 'is_active', 'is_default_checked' => true])
                    </fieldset>

                    <button class="btn btn-primary" type="submit">Generate Page with Converter</button>
                </form>

            </div>
        </div>
    </div>

@endsection