@extends('backend.layouts.master')

@section('title', 'Table generator')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Table generator</span></h4>
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Table generator'])
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/editors/ace/ace.js') }}"></script>
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Table generator form'])

            <div class="panel-body" id="table-generator">
                <form action="#" class="form-horizontal">
                    <fieldset class="content-group">

                        <legend class="text-bold">Basic data</legend>

                        <div class="form-group">
                            <label class="control-label col-lg-2">From</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="From" name="from" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">To</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="To" name="to" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Multiplier</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Multiplier" name="multiplier" class="form-control"/>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset class="content-group">

                        <legend class="text-bold">Ranges</legend>

                        <div id="rows">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Range</label>
                                <div class="col-lg-3">
                                    <input value="1" min="0" type="number" placeholder="Start" name="start[]" class="form-control"/>
                                </div>
                                <div class="col-lg-3">
                                    <input value="19" min="0" type="number" placeholder="Finish" name="finish[]" class="form-control"/>
                                </div>
                                <div class="col-lg-3">
                                    <input value="1" min="0" type="text" placeholder="Step" name="step[]" class="form-control"/>
                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-danger remove-button"><i class="icon-close2"></i></button>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-success add-button">Add</button>

                    </fieldset>

                    <input type="submit" value="Generate" class="btn btn-primary submit" />
                    <input type="button" value="Show HTML" class="btn btn-default show-html hidden" />
                </form>
            </div>
        </div>

        <div id="html"></div>

        <div id="results"></div>

        <script>
            (function () {
                var $generator = $('#table-generator');
                var $results = $('#results');
                var $html = $('#html');
                var $rows = $('#rows');

                $generator.on('click', '.show-html', function() {
                    var htmlData = $('.panel-body', $results).html();

                    $html.html('<textarea class="form-control" rows="6"></textarea>');
                    $('textarea', $html).text('<div class="row">' + htmlData + '</div>');

                    $(this).removeClass('show-html').addClass('hide-html').val('Hide HTML');
                    return false;
                });

                $generator.on('click', '.hide-html', function() {
                    $html.empty();
                    $(this).removeClass('hide-html').addClass('show-html').val('Show HTML');
                    return false;
                });

                $generator.on('click', '.remove-button', function() {
                    $(this).closest('.form-group').remove();
                    return false;
                });

                $generator.on('click', '.add-button', function() {
                    $rows.append(
                        '<div class="form-group">\n' +
'                            <label class="control-label col-lg-2">Range</label>\n' +
'                            <div class="col-lg-3">\n' +
'                                <input type="number" min="0" placeholder="Start" name="start[]" class="form-control"/>\n' +
'                            </div>\n' +
'                            <div class="col-lg-3">\n' +
'                                <input type="number" min="0" placeholder="Finish" name="finish[]" class="form-control"/>\n' +
'                           </div>\n' +
'                           <div class="col-lg-3">\n' +
'                               <input type="text" placeholder="Step" name="step[]" class="form-control"/>\n' +
'                           </div>\n' +
'                           <div class="col-lg-1">\n' +
'                               <button class="btn btn-danger remove-button"><i class="icon-close2"></i></button>\n' +
'                           </div>\n' +
'                       </div>'
                    );
                    return false;
                });

                $generator.on('click', '.submit', function () {
                    var from = $('[name="from"]').val();
                    var to = $('[name="to"]').val();
                    var multiplier = $('[name="multiplier"]').val().replace(',', '.');

                    var startValues = $('[name^="start"]').map(function (i, item) {
                        return $(item).val();
                    }).get();

                    var finishValues = $('[name^="finish"]').map(function (i, item) {
                        return $(item).val();
                    }).get();

                    var stepValues = $('[name^="step"]').map(function (i, item) {
                        return $(item).val();
                    }).get();

                    console.log(startValues, finishValues, stepValues);

                    var results = generateData({
                        "from": from,
                        "to": to,
                        "multiplier": multiplier,
                        "startValues": startValues,
                        "finishValues": finishValues,
                        "stepValues": stepValues
                    });

                    var tables = generateTables(results, 2, {
                        "tableClass": "table table-bordered",
                        "tableWrapper": "<div class='col-lg-6'>",
                        "tableWrapperClose": "</div>"
                    });

                    $results.html('<div class="panel panel-flat"><div class="panel-body">' + tables + '</div></div>');

                    $('.show-html').removeClass('hidden');

                    return false;
                });

                function generateData(params) {
                    var data = {};
                    var k = 0;
                    for (var i = 0; i < params.startValues.length; i++) {
                        for (var j = 0; j < +params.finishValues[i]; j++) {
                            var fromValue = (+params.startValues[i] + (j * +params.stepValues[i]));

                            data[k++] = {
                                "key": round(fromValue) + ' ' + params.from + ' to ' + params.to,
                                "value": round(fromValue * +params.multiplier)
                            };
                        }
                    }

                    return data;
                }

                function generateTables(data, tablesCount, options) {
                    var tableLength = Math.round(Object.keys(data).length / tablesCount);
                    var tables = '';
                    var k = 0;

                    for (var i = 0; i < tablesCount; i++) {
                        tables += options.tableWrapper;
                        tables += '<table class="' + options.tableClass + '">';
                        for (var j = 0; j < tableLength; j++) {
                            if(data[k]) {
                                tables += '<tr>';
                                tables += '<td>' + data[k]['key'] + '</td>';
                                tables += '<td>' + data[k]['value'] + '</td>';
                                tables += '</tr>';
                            }
                            k++;
                        }
                        tables += '</table>';
                        tables += options.tableWrapperClose;
                    }

                    return tables;
                }

                function round(digit) {
                    return Math.round(Number(digit) * 10000) / 10000;
                }
            })();
        </script>
    </div>
@endsection
