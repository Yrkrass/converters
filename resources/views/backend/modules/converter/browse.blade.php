@php
    /**
     * @var $converter \App\Models\Converter
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Converters')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Converters</span> - list of converters</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'converter', 'route' => 'admin.converters.create'])
    @include('backend.components.create-new', ['name' => 'converters', 'route' => 'admin.converters.mass-create', 'icon' => 'icon-folder-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Converters'])
@endsection

@section('content')
    <div class="panel panel-flat">

        @include('backend.components.panel-heading', ['title' => 'Converters data table', 'count' => $convertersCount])

        <div class="dataTables_wrapper">
            <div class="datatable-header">
                <form action="{{ route('admin.converters.index') }}" method="GET">
                    <div class="form-group col-sm-4">
                        <label>Type</label>
                        <select class="form-control" name="type" onchange="$(this).closest('form').submit();">
                            <option value="0">All</option>
                            @foreach($types as $type)
                                <option {{ $selectedType == $type ? ' selected' : '' }} value="{{ $type }}">{{ str_name($type) }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <table class="table datatable-html table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Created</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($converters as $converter)
                    <tr>
                        <td>{{ $converter->id }}</td>
                        <td>
                            <a href="{{ route('admin.converters.edit', ['converter' => $converter->id]) }}">{{ $converter->name }}</a>
                        </td>
                        <td>{!! $converter->getDescription() !!}</td>
                        <td>
                            <a href="{{ route('admin.converters.index', ['type' => $converter->type]) }}" class="label label-info">{{ str_name($converter->type) }}</a>
                        </td>
                        <td>{{ $converter->created_at }}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a data-original-title="Edit" data-popup="tooltip" href="{{ route('admin.converters.edit', ['converter' => $converter->id]) }}"><i class="icon-pencil5 text-blue"></i></a>
                                </li>
                                <li>
                                    <a data-original-title="Delete" data-popup="tooltip" data-href="{{ route('admin.converters.destroy', ['converter' => $converter->id]) }}" data-method="delete" class="jquery-postback"><i class="icon-trash text-danger"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
