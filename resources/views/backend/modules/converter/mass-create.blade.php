@php
    /**
     * @var $converter \App\Models\Converter
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Converter')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Converter</span> - mass create
    </h4>
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'converter', 'route' => 'admin.converters.create'])
    @include('backend.components.create-new', ['name' => 'converters', 'route' => 'admin.converters.mass-create', 'icon' => 'icon-folder-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Converter', 'elements' => [
        'admin.converters.index' => 'Converters'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit Converter data'])

            <div class="panel-body">

                <form action="{{ route('admin.converters.mass-store') }}" method="post" class="form-horizontal">

                    @csrf

                    <fieldset class="content-group">

                        <div class="form-group">
                            <label class="control-label col-lg-2">Data array <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <textarea name="converters" rows="5" cols="5" class="form-control"></textarea>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">Create Converters</button>

                    </fieldset>
                </form>
            </div>
        </div>
    </div>

@endsection