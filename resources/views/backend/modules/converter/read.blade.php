@php
/**
 * @var $converter \App\Models\Converter
 */
@endphp

@extends('backend.layouts.master')

@section('title', 'Converter')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Converter</span> - {{ $actionMode }}
    </h4>
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'converter', 'route' => 'admin.converters.create'])
    @include('backend.components.create-new', ['name' => 'converters', 'route' => 'admin.converters.mass-create', 'icon' => 'icon-folder-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Converter', 'elements' => [
        'admin.converters.index' => 'Converters'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit Converter data'])

            <div class="panel-body">

                <form action="{{ $converter->exists ? route('admin.converters.update', ['user' => $converter->id]) : route('admin.converters.store') }}"
                      method="post" class="form-horizontal">

                    @csrf

                    @if($converter->exists) {{ method_field('PUT') }} @endif

                    <input type="hidden" name="id" value="{{ $converter->id }}"/>

                    <fieldset class="content-group">

                        @if($converter->exists)

                        @include('backend.components.form.timestamps', [
                            'model' => $converter,
                        ])

                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">Type <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <select name="type" class="select">
                                    @foreach($converterTypes as $item)
                                        <option {{ $item == $converter->type ? 'selected=selected' : '' }} value="{{ $item }}">{{ ucwords(str_replace('_', ' ', $item)) }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('type'))
                                    <label class="validation-error-label">{{ $errors->first('type') }}</label>
                                @endif
                            </div>
                        </div>

                        @include('backend.components.form.text-field', [
                            'name' => 'name',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $converter,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'from',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $converter,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'to',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $converter,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'from_callback',
                            'is_required' => true,
                            'title' => 'To = ',
                            'errors' => $errors,
                            'model' => $converter,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'to_callback',
                            'title' => 'From = ',
                            'errors' => $errors,
                            'model' => $converter,
                        ])

                        <div class="form-group">
                            <label class="control-label col-lg-2">From Input Type</label>
                            <div class="col-lg-10">
                                <select name="from_type" class="select">
                                    @foreach($inputTypes as $item)
                                        <option {{ $item == $converter->from_type ? 'selected=selected' : '' }} value="{{ $item }}">{{ str_name($item) }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('from_type'))
                                    <label class="validation-error-label">{{ $errors->first('from_type') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">To Input Type</label>
                            <div class="col-lg-10">
                                <select name="to_type" class="select">
                                    @foreach($inputTypes as $item)
                                        <option {{ $item == $converter->to_type ? 'selected=selected' : '' }} value="{{ $item }}">{{ str_name($item) }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('to_type'))
                                    <label class="validation-error-label">{{ $errors->first('to_type') }}</label>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">{{ $actionMode }} Converter</button>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>

@endsection
