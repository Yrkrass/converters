<?php
    /**
     * @var $user \App\Models\User;
     * @var $actionMode string
     */
?>

@extends('backend.layouts.master')

@section('title', 'User')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">User</span> - {{ $actionMode }}</h4>
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'user', 'route' => 'admin.users.create', 'icon' => 'icon-user-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'User', 'elements' => [
        'admin.users.index' => 'Users'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit User data'])

            <div class="panel-body">

                <form action="{{ $user->exists ? route('admin.users.update', ['user' => $user->id]) : route('admin.users.store') }}" method="post" class="form-horizontal">

                    @csrf

                    @if($user->exists) {{ method_field('PUT') }} @endif

                    <input type="hidden" name="id" value="{{ $user->id }}"/>

                    <fieldset class="content-group">

                        @if($user->exists)

                        @include('backend.components.form.timestamps', [
                            'model' => $user,
                        ])

                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="text" name="name" placeholder="Name" value="{{ old('name') ?: $user->name }}" class="form-control">
                                </div>
                                @if($errors->has('name'))
                                    <label class="validation-error-label">{{ $errors->first('name') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Email <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-mail5"></i></span>
                                    <input type="text" name="email" placeholder="Email address" value="{{ old('email') ?: $user->email }}" class="form-control">
                                </div>
                                @if($errors->has('email'))
                                    <label class="validation-error-label">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Password
                                @if(!$user->exists)
                                    <span class="text-danger">*</span>
                                @endif
                            </label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-lock2"></i></span>
                                    <input type="password" name="password" placeholder="Password" class="form-control">
                                </div>
                                @if($errors->has('password'))
                                    <label class="validation-error-label">{{ $errors->first('password') }}</label>
                                @endif
                            </div>
                        </div>

                        @include('backend.components.form.checkbox', [
                            'name' => 'is_active',
                            'is_default_checked' => true,
                            'model' => $user,
                        ])

                        <button class="btn btn-primary" type="submit">{{ $actionMode }} User</button>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>

@endsection
