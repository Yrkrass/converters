<?php
/**
 * @var $user \App\Models\User
 */
?>

@extends('backend.layouts.master')

@section('title', 'Users')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span> - list of registered users</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'user', 'route' => 'admin.users.create', 'icon' => 'icon-user-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Users'])
@endsection

@section('content')
    <div class="panel panel-flat">

        @include('backend.components.panel-heading', ['title' => 'Users data table', 'count' => $usersCount])

        <table class="table datatable-html table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Created</th>
                <th>Active</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>
                        <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}">{{ $user->name }}</a>
                    </td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        @if($user->is_active)
                            <span class="label label-success">Active</span>
                        @else
                            <span class="label label-default">Inactive</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('admin.users.edit', ['user' => $user->id]) }}"><i class="icon-database-edit2"></i> Edit</a></li>
                                    @if($user->id != auth()->user()->id)
                                        <li>
                                            <a data-href="{{ route('admin.users.destroy', ['user' => $user->id]) }}" data-method="delete" class="jquery-postback"><i class="icon-close2"></i> Delete</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
