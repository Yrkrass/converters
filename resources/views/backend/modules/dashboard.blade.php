@extends('backend.layouts.master')

@section('title', 'Dashboard')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dashboard</span> - General information</h4>
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs')
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="container-fluid">
            Dashboard content
        </div>
    </div>
@endsection
