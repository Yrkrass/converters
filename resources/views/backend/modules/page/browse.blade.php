@php
    /**
     * @var $page \App\Models\Page
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Pages')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pages</span> - list of registered pages</h4>
@endsection

@section('heading-elements')
    <a href="{{ route('admin.generate.sitemap') }}" class="btn btn-success">
        <i class="position-left"></i>Generate sitemap
    </a>
    @include('backend.components.create-new', ['name' => 'page', 'route' => 'admin.pages.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Pages'])
@endsection

@section('content')
    <div class="panel panel-flat">

        @include('backend.components.panel-heading', ['title' => 'Pages data table', 'count' => $pagesCount])

        <div class="dataTables_wrapper">
            <div class="datatable-header">
                <form action="{{ route('admin.pages.index') }}" method="GET">
                    <div class="form-group col-sm-4">
                        <label>Group</label>
                        <select class="form-control" name="group_id" onchange="$(this).closest('form').submit();">
                            <option value="0">All</option>
                            <option {{ $groupId == '-1' ? ' selected' : '' }} value="-1">--</option>
                            @foreach($groups as $group)
                                <option {{ $groupId == $group->id ? ' selected' : '' }} value="{{ $group->id }}">[{{ $group->pages->count() }}] {{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <table class="table datatable-html table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Link</th>
                    <th>Title</th>
                    <th>Group</th>
                    <th>SEO</th>
                    <th>Created</th>
                    <th>Active</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td><a data-original-title="View on site" data-popup="tooltip" href="{{ route('page', $page->slug) }}" target="_blank"><i class="icon-link" style="color: {{ rand_color($page->id) }}"></i></a></td>
                        <td>
                            <a href="{{ route('admin.pages.edit', ['page' => $page->id]) }}">{{ $page->title }}</a>
                        </td>
                        <td>
                            @isset($page->group)
                                <a href="{{ route('admin.page-groups.edit', ['id' => $page->page_group_id]) }}" class="label label-info">{{ $page->group->name }}</a>
                            @else
                                --
                            @endisset
                        </td>
                        <td>
                            <span data-original-title="SEO Title" data-popup="tooltip" class="label label-{{ $page->seo_title ? 'success' : 'default' }}">T</span>
                            <span data-original-title="SEO Keywords" data-popup="tooltip" class="label label-{{ $page->seo_keywords ? 'success' : 'default' }}">K</span>
                            <span data-original-title="SEO Description" data-popup="tooltip" class="label label-{{ $page->seo_description ? 'success' : 'default' }}">D</span>
                        </td>
                        <td>{{ $page->created_at }}</td>
                        <td>
                            @if($page->is_active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-default">Inactive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a data-original-title="Edit" data-popup="tooltip" href="{{ route('admin.pages.edit', ['id' => $page->id]) }}"><i class="icon-pencil5 text-blue"></i></a>
                                </li>
                                <li>
                                    <a data-original-title="Delete" data-popup="tooltip" data-href="{{ route('admin.pages.destroy', ['id' => $page->id]) }}" data-method="delete" class="jquery-postback"><i class="icon-trash text-danger"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
