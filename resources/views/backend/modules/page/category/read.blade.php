@php
    /**
     * @var $pageCategory \App\Models\PageCategory
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Page')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page Category</span> - {{ $actionMode }}</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'Page Category', 'route' => 'admin.page-categories.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page Category', 'elements' => [
        'admin.page-categories.index' => 'Page Categories'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit Page Category data'])

            <div class="panel-body">

                <form action="{{ $pageCategory->exists ? route('admin.page-categories.update', ['pageCategory' => $pageCategory->id]) : route('admin.page-categories.store') }}" method="post" class="form-horizontal">

                    @csrf

                    @if($pageCategory->exists) {{ method_field('PUT') }} @endif

                    <input type="hidden" name="id" value="{{ $pageCategory->id }}"/>

                    <fieldset class="content-group">

                        @if($pageCategory->exists)
                            @include('backend.components.form.timestamps', ['model' => $pageCategory])
                        @endif

                        @include('backend.components.form.text-field', [
                            'name' => 'name',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $pageCategory,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'slug',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $pageCategory,
                        ])

                        <button class="btn btn-primary" type="submit">{{ $actionMode }} Page Category</button>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>

@endsection
