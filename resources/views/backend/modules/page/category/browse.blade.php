@php
    /**
     * @var $category \App\Models\PageCategory
     */
@endphp


@extends('backend.layouts.master')

@section('title', 'Page Categories')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page Categories</span> - list of page categories</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'Page Category', 'route' => 'admin.page-categories.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page Categories'])
@endsection

@section('content')
    <div class="panel panel-flat">

        @include('backend.components.panel-heading', ['title' => 'Page Categories data table', 'count' => $pageCategoriesCount])

        <div class="dataTables_wrapper">
            <div class="datatable-header">
                <form action="{{ route('admin.page-categories.index') }}" method="GET">
                    <div class="form-group col-sm-4">
                        <label>Group</label>
                        <select class="form-control" name="group_id" onchange="$(this).closest('form').submit();">
                            <option value="0">All</option>
                            @foreach($groups as $group)
                                <option {{ $groupId == $group->id ? ' selected' : '' }} value="{{ $group->id }}">[{{ $group->categories->count() }}] {{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <table class="table datatable-html table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Groups</th>
                    <th>Created</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pageCategories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>
                            <a href="{{ route('admin.page-categories.edit', ['pageCategory' => $category->id]) }}">{{ $category->name }}</a>
                        </td>
                        <td><a href="{{ route('admin.page-groups.index', ['category_id' => $category->id]) }}" class="label label-{{ $category->groups->count() > 0 ? 'success' : 'default' }}">{{ $category->groups->count() }}</a></td>
                        <td>{{ $category->created_at }}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a data-original-title="Edit" data-popup="tooltip" href="{{ route('admin.page-categories.edit', ['id' => $category->id]) }}"><i class="icon-pencil5 text-blue"></i></a>
                                </li>
                                <li>
                                    <a data-original-title="Delete" data-popup="tooltip" data-href="{{ route('admin.page-categories.destroy', ['id' => $category->id]) }}" data-method="delete" class="jquery-postback"><i class="icon-trash text-danger"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
