@php
    /**
     * @var $pageGroup \App\Models\PageGroup
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Page')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page Group</span> - {{ $actionMode }}</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'Page Group', 'route' => 'admin.page-groups.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page Group', 'elements' => [
        'admin.page-groups.index' => 'Page Groups'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit Page Group data'])

            <div class="panel-body">

                <form action="{{ $pageGroup->exists ? route('admin.page-groups.update', ['pageGroup' => $pageGroup->id]) : route('admin.page-groups.store') }}" method="post" class="form-horizontal">

                    @csrf

                    @if($pageGroup->exists) {{ method_field('PUT') }} @endif

                    <input type="hidden" name="id" value="{{ $pageGroup->id }}"/>

                    <fieldset class="content-group">

                        @if($pageGroup->exists)
                            @include('backend.components.form.timestamps', ['model' => $pageGroup])
                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">Categories</label>
                            <div class="col-lg-10">
                                <select name="categories[]" multiple="multiple" class="select">
                                    @foreach($categories as $category)
                                        <option {{ in_array($category->id, $selectedCategoryIds) ? 'selected=selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @include('backend.components.form.text-field', [
                            'name' => 'name',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $pageGroup,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'slug',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $pageGroup,
                        ])

                        <button class="btn btn-primary" type="submit">{{ $actionMode }} Page Group</button>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>

@endsection
