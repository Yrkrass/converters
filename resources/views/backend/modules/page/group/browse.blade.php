@php
    /**
     * @var $group \App\Models\PageGroup
     */
@endphp


@extends('backend.layouts.master')

@section('title', 'Page Groups')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page Groups</span> - list of page groups</h4>
@endsection

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'Page Group', 'route' => 'admin.page-groups.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page Groups'])
@endsection

@section('content')
    <div class="panel panel-flat">

        @include('backend.components.panel-heading', ['title' => 'Page Groups data table', 'count' => $pageGroupsCount])

        <div class="dataTables_wrapper">
            <div class="datatable-header">
                <form action="{{ route('admin.page-groups.index') }}" method="GET">
                    <div class="form-group col-sm-4">
                        <label>Category</label>
                        <select class="form-control" name="category_id" onchange="$(this).closest('form').submit();">
                            <option value="0">All</option>
                            @foreach($categories as $category)
                                <option {{ $categoryId == $category->id ? ' selected' : '' }} value="{{ $category->id }}">[{{ $category->groups->count() }}] {{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <table class="table datatable-html table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Categories</th>
                    <th>Pages</th>
                    <th>Created</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pageGroups as $group)
                    <tr>
                        <td>{{ $group->id }}</td>
                        <td>
                            <a href="{{ route('admin.page-groups.edit', ['pageGroup' => $group->id]) }}">{{ $group->name }}</a>
                        </td>
                        <td><a href="{{ route('admin.page-categories.index', ['group_id' => $group->id]) }}" class="label label-{{ $group->categories_count > 0 ? 'success' : 'default' }}">{{ $group->categories_count }}</a></td>
                        <td><span class="label label-{{ $group->pages_count > 0 ? 'success' : 'default' }}">{{ $group->pages_count }}</span></td>
                        <td>{{ $group->created_at }}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a data-original-title="Edit" data-popup="tooltip" href="{{ route('admin.page-groups.edit', ['id' => $group->id]) }}"><i class="icon-pencil5 text-blue"></i></a>
                                </li>
                                <li>
                                    <a data-original-title="Delete" data-popup="tooltip" data-href="{{ route('admin.page-groups.destroy', ['id' => $group->id]) }}" data-method="delete" class="jquery-postback"><i class="icon-trash text-danger"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
