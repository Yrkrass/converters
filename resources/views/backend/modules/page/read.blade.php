@php
    /**
     * @var $page \App\Models\Page
     */
@endphp

@extends('backend.layouts.master')

@section('title', 'Page')

@section('page-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Page</span> - {{ $actionMode }}</h4>
@endsection

@push('scripts-header')
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/pages/form_checkboxes_radios.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cms/js/plugins/editors/summernote/summernote.min.js') }}"></script>
@endpush

@push('scripts-header')
    <script>
        (function($) {
            setTimeout(function() {
                $('.summernote').summernote({
                    hint: {
                        mentions: [{!! $convertersString !!}],
                        match: /\B@(\w*)$/,
                        search: function (keyword, callback) {
                            callback($.grep(this.mentions, function (item) {
                                return item.indexOf(keyword) == 0;
                            }));
                        },
                        content: function (item) {
                            return '@' + item;
                        }
                    }
                });
            }, 500);
        })(jQuery);
    </script>
@endpush

@section('heading-elements')
    @include('backend.components.create-new', ['name' => 'page', 'route' => 'admin.pages.create', 'icon' => 'icon-file-plus'])
@endsection

@section('breadcrumbs')
    @include('backend.components.breadcrumbs', ['current' => 'Page', 'elements' => [
        'admin.pages.index' => 'Pages'
    ]])
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="panel panel-flat">
            @include('backend.components.panel-heading', ['title' => 'Edit Page data'])

            <div class="panel-body">

                <form action="{{ $page->exists ? route('admin.pages.update', ['page' => $page->id]) : route('admin.pages.store') }}" method="post" class="form-horizontal">

                    @csrf

                    @if($page->exists) {{ method_field('PUT') }} @endif

                    <input type="hidden" name="id" value="{{ $page->id }}"/>

                    <fieldset class="content-group">

                        @if($page->exists)
                            @include('backend.components.form.timestamps', ['model' => $page])
                        @endif

                        @include('backend.components.form.select', [
                            'name' => 'page_group_id',
                            'title' => 'Group',
                            'errors' => $errors,
                            'model' => $page,
                            'items' => $groups,
                            'allow_null' => true,
                            'selected_value' => old('page_group_id') ?? $page->page_group_id,
                            'item_name' => 'name',
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'slug',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $page,
                        ])

                        @include('backend.components.form.text-field', [
                            'name' => 'title',
                            'is_required' => true,
                            'errors' => $errors,
                            'model' => $page,
                        ])

                        @include('backend.components.form.text-area', [
                            'name' => 'seo_title',
                            'errors' => $errors,
                            'model' => $page,
                        ])

                        @include('backend.components.form.text-area', [
                            'name' => 'seo_description',
                            'errors' => $errors,
                            'model' => $page,
                        ])

                        @include('backend.components.form.text-area', [
                            'name' => 'seo_keywords',
                            'errors' => $errors,
                            'model' => $page,
                        ])

                        @include('backend.components.form.checkbox', [
                            'name' => 'is_active',
                            'is_default_checked' => true,
                            'model' => $page,
                        ])

                        <textarea name="content" class="summernote">{!! old('content') ?: $page->content !!}</textarea>

                        <button class="btn btn-primary" type="submit">{{ $actionMode }} Page</button>
                        @if($page->exists)
                        <a class="btn btn-success" target="_blank" href="{{ route('page', $page->slug) }}">View on site</a>
                        @endif
                    </fieldset>
                </form>

            </div>
        </div>
    </div>

@endsection
