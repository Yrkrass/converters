<?php
/**
 * @var $newSitesCount string
 */

$user = auth()->user();
?>

<!DOCTYPE html>
<html>
<head>
    @section('head')
        <title>{{ config('app.name', '') }} / Admin - @yield('title', '')</title>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="@yield('meta-description', '')"/>
        <meta name="keywords" content="@yield('meta-keywords', '')"/>

        {{--<meta name="referrer" content="origin-when-crossorigin" id="meta_referrer"/>--}}
        <meta name="referrer" content="none">

        <link rel="canonical" href="@yield('canonical', url()->current())"/>

        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <meta name="referrer" content="none">

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

        <link href="{{ asset('cms/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/colors.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('cms/css/style.css') }}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="{{ asset('cms/js/plugins/loaders/pace.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/core/libraries/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/core/libraries/bootstrap.min.js') }}"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="{{ asset('cms/js/plugins/ui/nicescroll.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/uniform.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switchery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/styling/switch.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('cms/js/plugins/tables/datatables/datatables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/plugins/tables/datatables/extensions/select.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/plugins/forms/selects/select2.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('cms/js/core/app.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/pages/datatables_data_sources.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/pages/datatables_extension_select.js') }}"></script>
        <script type="text/javascript" src="{{ asset('cms/js/pages/form_select2.js') }}"></script>

        <script type="text/javascript" src="{{ asset('cms/js/pages/layout_fixed_custom.js') }}"></script>

        <script type="text/javascript" src="{{ asset('cms/js/app.js') }}"></script>

        <!-- /theme JS files -->


        <!-- sweetalert -->
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>
        <!-- /sweetalert -->

        <script>
            $(document).on('click', '.sweet-confirm', function(e) {
                console.log(e);

                if(e.isTrigger) {
                    return;
                }

                e.preventDefault();
                e.stopPropagation();

                var $this = $(this);
                var href = $this.data('href');

                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6daccf',
                    confirmButtonText: 'Continue',
                    cancelButtonText: 'Cancel',
                }).then((result) => {
                    if (result.value) {
                        $this.attr('href', href);
                        $this.click();
                    }
                });

                return false;
            });
        </script>

        @stack('styles-header')
        @stack('scripts-header')
    @show
</head>
<body class="navbar-top">


@section('body')
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}" target="_blank">
                Converters Manager
                <sup>beta</sup>
            </a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
            <p class="navbar-text">
                {{--<span class="label bg-success">Online</span>--}}
            </p>

            <ul class="nav navbar-nav navbar-right">
                <li {{ highlight_menu_item('users') }}><a href="{{ route('admin.users.index') }}"><i class="icon-people"></i></a></li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('cms/images/image.png') }}" alt="">
                        <span>{{ Auth::user()->name }}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('admin.users.edit', ['user' => Auth::user()->id]) }}"><i class="icon-user"></i> My profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i>Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}" class="media-left"><img src="{{ asset('cms/images/image.png') }}" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold">{{ $user->name }}</span>
                                    <div class="text-size-mini text-muted">
                                        <i class="icon-vcard text-size-small"></i> &nbsp Administrator
                                    </div>
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        {{--<li>--}}
                                            {{--<a href="#"><i class="icon-cog3"></i></a>--}}
                                        {{--</li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->

                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <!-- Main -->
                                <li class="navigation-header">
                                    <span>Main</span> <i class="icon-menu" title="Main pages"></i>
                                </li>

                                @include('backend.components.main-menu', ['items' => [
                                    [
                                        'route' => 'admin.index',
                                        'title' => 'Dashboard',
                                        'icon' => 'icon-home4'
                                    ],
                                    [
                                        'route' => 'admin.converters.index',
                                        'title' => 'Converters',
                                        'icon' => 'icon-transmission',
                                        'count' => $convertersCount,
                                    ],
                                    [
                                        'title' => 'Pages',
                                        'icon' => 'icon-file-empty',
                                        'children' => [
                                            [
                                                'route' => 'admin.pages.index',
                                                'title' => 'Pages',
                                                'count' => $pagesCount,
                                            ],
                                            [
                                                'route' => 'admin.page-categories.index',
                                                'title' => 'Page Categories',
                                                'count' => $pageCategoriesCount,
                                            ],
                                            [
                                                'route' => 'admin.page-groups.index',
                                                'title' => 'Page Groups',
                                                'count' => $pageGroupsCount,
                                            ],
                                        ]
                                    ],
                                    [
                                        'title' => 'Tools',
                                        'icon' => 'icon-cog5',
                                        'children' => [
                                            [
                                                'route' => 'admin.generators.table',
                                                'title' => 'Table generator',
                                            ],
                                            [
                                                'route' => 'admin.generators.page',
                                                'title' => 'Page generator',
                                            ],
                                        ]
                                    ],
                                ]])
                            <!-- /main -->
                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            @yield('page-title')
                        </div>

                        <div class="heading-elements">
                            @yield('heading-elements')
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        @yield('breadcrumbs')
                    </div>
                </div>
                <!-- /page header -->

                <!-- Content area -->
                <div class="content">

                @include('backend.components.alert-displayer')

                @yield('content')

                <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; {{ date('Y') }}. All Rights Reserved.
                    </div>
                    <!-- /footer -->
                </div>
                <!-- /content area -->
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover({
                html: true
            });
        })
    </script>

    <script>
        window.ROOT_PATH = "{!! url('/') !!}";
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
    </script>

    @stack('styles-footer')
    @stack('scripts-footer')
@show
</body>
</html>
