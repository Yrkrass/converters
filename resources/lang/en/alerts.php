<?php

return [
    'success' => [
        'created' => 'Successfully created',
        'updated' => 'Successfully updated',
        'deleted' => 'Successfully deleted',
    ],
    'error' => [
        'created' => 'Create error',
        'updated' => 'Update error',
        'deleted' => 'Delete error',
    ],
    'info' => [
        'empty_data_table' => 'Data table has no records'
    ]
];