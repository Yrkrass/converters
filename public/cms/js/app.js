(function ($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {

        $(document).on('click', '[data-method="delete"]', function(e) {
            if(confirm('Are you sure?')) {
                e.preventDefault();

                var $this = $(this);

                ajaxSend($this.data('method'), $this.data('href'), [], function (response) {
                    location.href = location.href;
                });
            }
            return false;
        });

        $(document).on('click', 'a.quick-change', function () {
            var $self = $(this);

            ajaxPost($self.attr('href'), { 'active' : $self.data('text') }, function (response) {
                var className = response.text ? 'green' : 'danger';
                $self.data('text', response.text);
                $('i', $self).removeClass('text-danger text-green').addClass('text-' + className);
            });
            return false;
        });
    });

    function ajaxPost(url, data, onsuccess, onfail, always) {
        ajaxSend('POST', url, data, onsuccess, onfail, always);
    }

    function ajaxGet(url, onsuccess, onfail, always) {
        ajaxSend('GET', url, [], onsuccess, onfail, always);
    }

    function ajaxSend(type, url, data, onsuccess, onfail, always) {
        $.ajax({
            type: type,
            url: url,
            data: data,//$form.serialize(),
            dataType: 'json',
            success: onsuccess
        }).fail(onfail).always(always);
    }
})(jQuery);
