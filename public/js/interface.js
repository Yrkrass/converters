(function($) {
    $(document).ready(function () {
        $('.treeview', document).each(function(i, item) {
            var $self = $(item);
            var name = $self.data('name');

            if(getCookie(name) && getCookie(name) == 1) {
                $self.removeClass('closed').find('ul').css('display', 'block');
            }
        });

        $('.treeview').on('click', '>a', function () {
            var $self = $(this).closest('.treeview');
            var $treeviewMenu = $('.treeview-menu', $self);
            var slideSpeed = 300;
            var name = $self.data('name');

            if ($self.hasClass('closed')) {
                $treeviewMenu.slideDown(slideSpeed);
                $self.removeClass('closed');
                setCookie(name, 1);
            }
            else {
                $treeviewMenu.slideUp(slideSpeed);
                $self.addClass('closed');
                setCookie(name, null);
            }
            return false;
        })
    });
})(jQuery);
