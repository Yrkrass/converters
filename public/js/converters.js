(function ($) {

    function send(url, data, onsuccess) {
        $.ajaxSetup({
            header: $('meta[name="csrf-token"]').attr('content')
        });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: onsuccess
        });
    }

    $(document).ready(function () {
        var container = '.converter';
        var $container = $(container);

        function round(digit, precision) {
            function getMultiplierByPrecision() {
                var arr = [1];
                for(var i = 0; i < precision; i++) {
                    arr.push(0);
                }
                return +arr.join('');
            }

            var multiplier = getMultiplierByPrecision(precision);

            return Math.round(Number(digit) * multiplier) / multiplier;
        }
        
        function calculatePrecision(callback) {
            var dig = callback(1);
            return dig.toString().replace(/[^-0-9]/gim,'').length;
        }
        
        function digitsAfterPoint(dig) {
            return dig.toString().includes('.') ? dig.toString().split('.').pop().length : 0;
        }

        function getConverterConfig(el, key) {
            var name = $(el).closest(container).data('name');
            return globals.converters[name][key];
        }

        function getFromCallback(el) {
            return getConverterConfig(el, 'fromCallback');
        }

        function getToCallback(el) {
            return getConverterConfig(el, 'toCallback');
        }

        function getResultString(el) {
            return getConverterConfig(el, 'result');
        }

        function getFromElement(el) {
            return $(el).closest('form').find('[name="from"]');
        }

        function getToElement(el) {
            return $(el).closest('form').find('[name="to"]');
        }

        function getResultElement(el) {
            return $(el).closest('form').find('.result');
        }

        function showResultLine(el, fromVal, toVal) {
            var resultString = getResultString(el);

            if (!fromVal || !toVal) return '';
            return '<b>Result: </b>' + resultString.replace('{from}', fromVal).replace('{to}', toVal);
        }

        (function() {
            $container.each(function () {
                var $this = $(this);

                if(!$this.text().length) {
                    send(globals.routes.converter, {"name": $this.data('name')}, function (response) {
                        $this.html(response);
                    });
                }
            });
        })();

        (function() {
            $container.on('submit', '[data-type="one_way"]', function () {
                var $form = $(this);

                send($form.attr('action'), $form.serialize(), function (response) {
                    var $result = $('.result', $form);

                    if(response && response.length) {
                        $result.html('<b>Result: </b>' + response);
                    }
                    else {
                        $result.empty();
                    }
                });

                return false;
            });
        })();

        (function() {
            $container.on('keyup', '[data-type="two_ways"] [name="from"], [data-type="two_ways"] [name="to"]', function () {
                var $self = $(this);
                var $form = $self.closest('form');
                var requestInputName = $self.attr('name');
                var responseInputName = requestInputName == 'from' ? 'to' : 'from';
                var $responseInput = $('[name="' + responseInputName + '"]', $form);

                send($form.attr('action'), {"name": requestInputName, "value": $self.val() }, function (response) {
                    var $result = $('.result', $form);

                    if(response && response.length && response > 0) {
                        $responseInput.val(response);

                        var from = response;
                        var to = $self.val();

                        if(requestInputName == 'from') {
                            from = $self.val();
                            to = response;
                        }

                        $result.html(showResultLine($self, from, to));
                    }
                    else {
                        $responseInput.val(null);
                        $result.empty();
                    }
                });
            });
        })();

        (function() {
            $container.on('click', '[type="reset"]', function () {
                getResultElement(this).empty();
            });

            $container.on('keyup input', '[data-type="multiplier"] [name="from"]', function () {
                var $from = getFromElement(this);
                var $to = getToElement(this);
                var $result = getResultElement(this);
                var fromCallback = getFromCallback(this);
                var val = fromCallback($(this).val());

                if(val == 0) {
                    $to.val(null);
                    getResultElement($to).empty();
                }
                else {
                    var precision = calculatePrecision(getToCallback(this));
                    $to.val(round(val, precision));
                }

                $result.html(showResultLine(this, $from.val(), $to.val()));
            });

            $container.on('keyup input', '[data-type="multiplier"] [name="to"]', function () {
                var $from = getFromElement(this);
                var $to = getToElement(this);
                var $result = getResultElement(this);
                var toCallback = getToCallback(this);
                var val = toCallback($(this).val());

                if(val == 0) {
                    $from.val(null);
                    getResultElement($from).empty();
                }
                else {
                    var precision = calculatePrecision(getFromCallback(this));
                    $from.val(round(val, precision));
                }

                $result.html(showResultLine(this, $from.val(), $to.val()));
            });
        })();
    });

})(jQuery);