<?php

namespace App\Providers;

use App\Models\Converter;
use App\Models\Page;
use App\Models\PageCategory;
use App\Models\PageGroup;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('backend.layouts.master', function ($view) {
            $view->with([
                'convertersCount' => Converter::count(),
                'pagesCount' => Page::count(),
                'pageCategoriesCount' => PageCategory::count(),
                'pageGroupsCount' => PageGroup::count(),
            ]);
        });

        View::composer('frontend.layouts.master', function ($view) {
            $view->with([
                'popularPages' => Page::active()->whereNotNull('page_group_id')->limit(5)->get(),
                'popularGroups' => PageGroup::limit(5)->get(),
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
