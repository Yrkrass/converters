<?php

namespace App\Providers;

use App\Models\Converter;
use App\Models\Page;
use App\Models\PageCategory;
use App\Models\PageGroup;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::pattern('pageSlug', '[0-9a-zA-Z\-_]+');
        Route::pattern('categorySlug', '[0-9a-zA-Z\-_]+');
        Route::pattern('groupSlug', '[0-9a-zA-Z\-_]+');
        Route::pattern('converterName', '[0-9a-zA-Z\-_]+');

        Route::model('pageSlug', Page::class);
        Route::bind('pageSlug', function ($value) {
            return $value instanceof Page ? $value : Page::where(['slug' => $value, 'is_active' => 1])->firstOrNew([]);
        });

        Route::model('categorySlug', PageCategory::class);
        Route::bind('categorySlug', function ($value) {
            return $value instanceof PageCategory ? $value : PageCategory::where(['slug' => $value])->firstOrNew([]);
        });

        Route::model('groupSlug', PageGroup::class);
        Route::bind('groupSlug', function ($value) {
            return $value instanceof PageGroup ? $value : PageGroup::where(['slug' => $value])->firstOrNew([]);
        });

        Route::model('converterName', Converter::class);
        Route::bind('converterName', function ($value) {
            return $value instanceof Converter ? $value : Converter::where(['name' => $value])->firstOrNew([]);
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
