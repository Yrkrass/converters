<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email|min:4|max:255',
            'subject' => 'min:2|max:255',
            'text' => 'required|min:3|max:10000',
        ];
    }
}