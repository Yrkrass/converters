<?php

namespace App\Http\Requests;

use App\Models\Converter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ConverterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $inputTypes = implode(',', Converter::INPUT_TYPES);

        return [
            'name' => "required|unique:converters,name,{$request->id}|min:2|max:255",
            'from' => 'required|min:2|max:255',
            'to' => 'required|min:2|max:255',
            'from_callback' => 'required|min:2|max:255',
            'to_callback' => 'min:0|max:255',
            'is_backend' => 'in:0,1',
            'type' => 'in:' . implode(',', Converter::TYPES),
            'from_type' => 'in:'. $inputTypes,
            'to_type' => 'in:'. $inputTypes,
        ];
    }
}
