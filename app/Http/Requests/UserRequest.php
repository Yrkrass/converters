<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Http\Request;

    class UserRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules(Request $request)
        {
            $rules = [
                'email' => "required|unique:users,email,{$request->id}|min:4|max:255",
                'name' => 'required|min:2|max:255',
                'is_active' => 'in:0,1',
            ];

            if ((filled($request->input('password')) && $request->id) || !$request->id) {
                $rules['password'] = 'required|min:3';
            }

            return $rules;
        }
    }
