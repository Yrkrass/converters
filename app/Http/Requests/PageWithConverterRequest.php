<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageWithConverterRequest extends FormRequest
{
    protected $redirectRoute = 'admin.generators.page';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'from' => 'required|min:1|max:255',
            'to' => 'required|min:1|max:255',
            'short_from' => 'required|min:1|max:255',
            'short_to' => 'required|min:1|max:255',
            'multiplier' => 'required|min:1|max:255',
            'example_num' => 'int|min:0',
            'is_from_greater' => 'in:0,1',
            'is_active' => 'in:0,1',
            'page_group_id' => 'required|int',
        ];
    }
}