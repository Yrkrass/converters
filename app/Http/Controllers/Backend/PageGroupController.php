<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageGroupRequest;
use App\Models\CategoryGroup;
use App\Models\PageGroup;
use App\Models\PageCategory;
use Illuminate\Http\Request;

class PageGroupController extends Controller
{
    public function index(Request $request)
    {
        $categoryId = intval($request->get('category_id'));

        $select = PageGroup::with('categories')->withCount('categories', 'pages');

        if($categoryId) {
            $ids = CategoryGroup::wherePageCategoryId($categoryId)->pluck('page_group_id');
            $select->whereIn('id', $ids);
        }

        $pageGroupsCount = $select->count();
        $pageGroups = $select->latest()->get();

        $categories = PageCategory::with('groups')->latest()->get();

        return view('backend.modules.page.group.browse', compact('pageGroups', 'pageGroupsCount', 'categories', 'categoryId'));
    }

    private function createEdit(PageGroup $pageGroup, $actionMode)
    {
        $categories = PageCategory::all();
        $selectedCategoryIds = $pageGroup->exists ? $pageGroup->categories()->pluck('page_category_id')->toArray() : [];

        return view('backend.modules.page.group.read', compact('pageGroup', 'actionMode', 'categories', 'selectedCategoryIds'));
    }

    public function create()
    {
        return $this->createEdit(new PageGroup(), 'Create');
    }

    public function edit(PageGroup $pageGroup)
    {
        return $this->createEdit($pageGroup, 'Edit');
    }

    public function update(PageGroupRequest $request, PageGroup $pageGroup)
    {
        return $this->save($request, $pageGroup);
    }

    public function store(PageGroupRequest $request)
    {
        return $this->save($request, new PageGroup());
    }

    public function destroy(PageGroup $pageGroup)
    {
        if ($pageGroup->exists) {
            $pageGroup->delete();
        }

        return ['status' => 'success', 'message' => __('alerts.success.deleted')];
    }

    private function save(PageGroupRequest $request, PageGroup $pageGroup)
    {
        $action = $pageGroup->exists ? 'updated' : 'created';

        $attributes = $request->all();

        $pageGroup->fill($attributes)->save();

        if(isset($attributes['categories'])) {
            $pageGroup->categories()->sync($attributes['categories']);
        }
        else {
            $pageGroup->categories()->detach();
        }

        return redirect(route('admin.page-groups.edit', $pageGroup->id))->with('message-success', __("alerts.success.$action"));
    }
}
