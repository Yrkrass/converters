<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageCategoryRequest;
use App\Models\CategoryGroup;
use App\Models\PageCategory;
use App\Models\PageGroup;
use Illuminate\Http\Request;

class PageCategoryController extends Controller
{
    public function index(Request $request)
    {
        $groupId = intval($request->get('group_id'));

        $select = PageCategory::with('groups');

        if($groupId) {
            $ids = CategoryGroup::wherePageGroupId($groupId)->pluck('page_category_id');
            $select->whereIn('id', $ids);
        }

        $pageCategoriesCount = $select->count();
        $pageCategories = $select->latest()->get();

        $groups = PageGroup::with('categories')->latest()->get();

        return view('backend.modules.page.category.browse', compact('pageCategories', 'pageCategoriesCount', 'groups', 'groupId'));
    }

    public function create()
    {
        $actionMode = 'Create';
        $pageCategory = new PageCategory();

        return view('backend.modules.page.category.read', compact('pageCategory', 'actionMode'));
    }

    public function edit(PageCategory $pageCategory)
    {
        $actionMode = 'Edit';

        return view('backend.modules.page.category.read', compact('pageCategory', 'actionMode'));
    }

    public function update(PageCategoryRequest $request, PageCategory $pageCategory)
    {
        return $this->save($request, $pageCategory);
    }

    public function store(PageCategoryRequest $request)
    {
        return $this->save($request, new PageCategory());
    }

    public function destroy(PageCategory $pageCategory)
    {
        if ($pageCategory->exists) {
            $pageCategory->delete();
        }

        return ['status' => 'success', 'message' => __('alerts.success.deleted')];
    }

    private function save(PageCategoryRequest $request, PageCategory $pageCategory)
    {
        $action = $pageCategory->exists ? 'updated' : 'created';

//        dd($request->all());

        $pageCategory->fill($request->all())->save();

        return redirect(route('admin.page-categories.edit', $pageCategory->id))->with('message-success', __("alerts.success.$action"));
    }
}
