<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConverterRequest;
use App\Models\Converter;
use Illuminate\Http\Request;

class ConverterController extends Controller
{
    public function index(Request $request)
    {
        $selectedType = $request->get('type');

        $select = Converter::latest();

        if($selectedType) {
            $select->whereType($selectedType);
        }

        $converters = $select->get();

        $convertersCount = $select->count();

        $types = Converter::TYPES;

        return view('backend.modules.converter.browse', compact('converters', 'convertersCount', 'types', 'selectedType'));
    }

    public function createEdit(Converter $converter, $actionMode)
    {
        $inputTypes = Converter::INPUT_TYPES;
        $converterTypes = Converter::TYPES;

        return view('backend.modules.converter.read', compact('converter', 'actionMode', 'inputTypes', 'converterTypes'));
    }

    public function create()
    {
        return $this->createEdit(new Converter(), 'Create');
    }

    public function edit(Converter $converter)
    {
        return $this->createEdit($converter, 'Edit');
    }

    public function update(ConverterRequest $request, Converter $converter)
    {
        return $this->save($request, $converter);
    }

    public function store(ConverterRequest $request)
    {
        return $this->save($request, new Converter());
    }

    public function destroy(Converter $converter)
    {
        if ($converter->exists) {
            $converter->delete();
        }

        return ['status' => 'success', 'message' => __('alerts.success.deleted')];
    }

    private function save(ConverterRequest $request, Converter $converter)
    {
        $action = $converter->exists ? 'updated' : 'created';

        $converter->fill($request->all())->save();

        return redirect(route('admin.converters.edit', $converter->id))->with('message-success', __("alerts.success.$action"));
    }

    public function massCreate()
    {
        return view('backend.modules.converter.mass-create');
    }

    public function massStore(Request $request)
    {
        $convertersString = $request->get('converters');
        $convertersDataArray = str_array($convertersString);

        $report = [];
        foreach ($convertersDataArray as $converterData) {
            $converter = Converter::firstOrNew($converterData);

            $status = 'exist';

            if(!$converter->exists) {
                $converter->save();
                $status = 'new';
            }
            $report[$converter->name] = $status;
        }

        $reportString = '<ul>';
        foreach($report as $key => $value) {
            $reportString .= '<li>' . $key . ' - ' . $value . '</li>';
        }
        $reportString .= '</ul>';

        return redirect(route('admin.converters.mass-create'))->with('message-success', $reportString);
    }
}








