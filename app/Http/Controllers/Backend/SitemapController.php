<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Page;

class SitemapController extends Controller
{
    public function generate()
    {
        $urls = [];

        $pages = Page::active()->get();

        foreach($pages as $page) {
            $urls[] = route('page', ['pageSlug' => $page->slug]);
        }

        \Storage::disk('global')->put('sitemap.xml', $this->getSitemap($urls));

        return redirect(route('admin.pages.index'))->with('message-success', 'ok');
    }

    private function getSitemap(array $urls)
    {
        $sitemap = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
        foreach($urls as $url) {
            $sitemap .=
    '<url><loc>' . $url . '</loc></url>' . "\n";
        }
        $sitemap .=
'</urlset>';
        return $sitemap;
    }
}