<?php

namespace App\Http\Controllers\Backend;

use App\Models\Generators\PageGenerator;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageWithConverterRequest;
use App\Models\Converter;
use App\Models\Page;
use App\Models\PageGroup;

class GeneratorController extends Controller
{
    public function table()
    {
        return view('backend.modules.generator.table');
    }


    public function page()
    {
        $groups = PageGroup::all();
        $convertersString = Converter::getConvertersString();
        $converterTypes = Converter::TYPES;
        $inputTypes = Converter::INPUT_TYPES;

        return view('backend.modules.generator.page', compact('groups', 'convertersString', 'converterTypes', 'inputTypes'));
    }

    public function pageStore(PageWithConverterRequest $request)
    {
        $attr = (object)$request->all();
//        dd($attr);

        $name = strtolower($attr->short_from . '-to-' . $attr->short_to);

        list($converter, $isConverterExists) = $this->storeConverter($name, $attr);
        list($page, $isPageExists) = $this->storePage($name, $attr);

        $reportString = '<ul>';
        $reportString .= '<li>Converter '. ($isConverterExists ? 'exists' : 'created') . ' | <a target="_blank" href="' . route('admin.converters.edit', $converter->id) . '">Edit</a></li>';
        $reportString .= '<li>Page '. ($isPageExists ? 'exists' : 'created') . ' | <a target="_blank" href="' . route('admin.pages.edit', $page->id) . '">Edit</a> | <a target="_blank" href="' . route('page', ['pageSlug' => $page->slug]) . '">View</a></li>';
        $reportString .= '</ul>';

        return redirect(route('admin.generators.page'))->with('message-success', $reportString);
    }

    private function storePage($slug, $attr)
    {
        $page = Page::firstOrNew(['slug' => $slug]);
        $isExists = true;

        if(!$page->exists) {
            $groupName = PageGroup::find($attr->page_group_id)->name;

            $generator = new PageGenerator($attr->from, $attr->to, $attr->short_from, $attr->short_to);

            $attr->slug = $slug;
            $attr->title = $generator->titleTemplate();
            $attr->seo_title = $generator->seoTitleTemplate();
            $attr->seo_description = $generator->seoDescriptionTemplate($groupName);
            $attr->content = $generator->contentTemplate(
                $slug,
                $attr->multiplier,
                $attr->is_from_greater,
                $attr->example_num,
                $attr->chart_table
            );

            $page->fill((array)$attr)->save();
            $isExists = false;
        }
        return [$page, $isExists];
    }

    private function storeConverter($name, $attr)
    {
        $converter = Converter::firstOrNew(['name' => $name]);
        $isExists = true;

        if(!$converter->exists) {
            $attr->name = $name;

            $attr->from_callback = Converter::getMultiplierFromCallback($attr->multiplier, $attr->is_from_greater);
            $attr->to_callback = Converter::getMultiplierToCallback($attr->multiplier, $attr->is_from_greater);

            $converter->fill((array)$attr)->save();
            $isExists = false;
        }
        return [$converter, $isExists];
    }
}
