<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $usersCount = User::count();
        $users = User::latest()->get();

        return view('backend.modules.user.browse', compact('users', 'usersCount'));
    }

    public function create()
    {
        $actionMode = 'Create';
        $user = new User();

        return view('backend.modules.user.read', compact('user', 'actionMode'));
    }

    public function edit(User $user)
    {
        $actionMode = 'Edit';

        return view('backend.modules.user.read', compact('user', 'actionMode'));
    }

    public function update(UserRequest $request, User $user)
    {
        return $this->save($request, $user);
    }

    public function store(UserRequest $request)
    {
        return $this->save($request, new User());
    }

    public function destroy(Request $request, User $user)
    {
        if ($user->exists && $user->id != $request->user()->id) {
            $user->delete();
        }

        return ['status' => 'success', 'message' => __('alerts.success.deleted')];//back()->with('message-success', __('alerts.success.deleted'));
    }

    private function save(UserRequest $request, User $user)
    {
        $action = $user->exists ? 'updated' : 'created';

        $attributes = $request->all();

        if ($attributes['password']) {
            $attributes['password'] = bcrypt($attributes['password']);
            $attributes['remember_token'] = str_random(10);
        } else {
            unset($attributes['password']);
        }

        $user->fill($attributes)->save();
//        $user->groups()->sync($attributes['groups']);

        return redirect(route('admin.users.edit', $user->id))->with('message-success', __("alerts.success.$action"));
    }
}
