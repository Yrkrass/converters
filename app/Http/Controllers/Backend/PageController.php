<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Models\Page;
use App\Models\PageGroup;
use App\Models\Converter;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $groupId = intval($request->get('group_id'));

        $select = Page::with('group');

        if($groupId == -1) {
            $select->whereNull('page_group_id');
        }
        else if($groupId) {
            $select->wherePageGroupId($groupId);
        }

        $pagesCount = $select->count();
        $pages = $select->latest()->get();
        $groups = PageGroup::with('pages')->latest()->get();

        return view('backend.modules.page.browse', compact('pages', 'pagesCount', 'groups', 'groupId'));
    }

    private function createEdit(Page $page, $actionMode)
    {
        $groups = PageGroup::all();
        $convertersString = Converter::getConvertersString();

        return view('backend.modules.page.read', compact('page', 'actionMode', 'groups', 'convertersString'));
    }

    public function create()
    {
        return $this->createEdit(new Page(), 'Create');
    }

    public function edit(Page $page)
    {
        return $this->createEdit($page, 'Edit');
    }

    public function update(PageRequest $request, Page $page)
    {
        return $this->save($request, $page);
    }

    public function store(PageRequest $request)
    {
        return $this->save($request, new Page());
    }

    public function destroy(Request $request, Page $page)
    {
        if ($page->exists) {
            $page->delete();
        }

        return ['status' => 'success', 'message' => __('alerts.success.deleted')];
    }

    private function save(PageRequest $request, Page $page)
    {
        $action = $page->exists ? 'updated' : 'created';

        $attributes = $request->all();

        if($attributes['page_group_id'] == 0) {
            unset($attributes['page_group_id']);
        }

        $page->fill($attributes)->save();

        return redirect(route('admin.pages.edit', $page->id))->with('message-success', __("alerts.success.$action"));
    }
}
