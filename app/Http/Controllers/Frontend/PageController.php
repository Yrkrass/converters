<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageCategory;
use App\Models\PageGroup;

class PageController extends Controller
{
    private $pageCategories;

    public function __construct()
    {
        $this->pageCategories = PageCategory::with('groups')->get();
    }
    
    public function index()
    {
        $page = Page::firstOrNew(['slug' => '/', 'is_active' => 1]);
        $pageCategories = $this->pageCategories;
        
        return view('frontend.index', compact('page', 'pageCategories'));
    }

    public function page(Page $page)
    {
        if(!$page->exists) {
            abort(404);
        }

        return view('frontend.page', ['page' => $page, 'pageCategories' => $this->pageCategories]);
    }

    public function pageCategory(PageCategory $pageCategory)
    {
        if(!$pageCategory->exists) {
            abort(404);
        }

        $page = Page::firstOrNew(['slug' => 'page-category', 'is_active' => 1]);

        return view('frontend.category', compact('page', 'pageCategory'));
    }

    public function pageGroup(PageGroup $pageGroup)
    {
        if(!$pageGroup->exists) {
            abort(404);
        }

        $page = Page::firstOrNew(['slug' => 'page-group', 'is_active' => 1]);

        return view('frontend.group', compact('page', 'pageGroup'));
    }

    public function sitemap()
    {
        $page = Page::firstOrNew(['slug' => 'sitemap', 'is_active' => 1]);
        $pageCategories = PageCategory::with('groups.pages')->get();

        return view('frontend.sitemap', compact('page', 'pageCategories'));
    }

    public function contacts()
    {
        $page = Page::firstOrNew(['slug' => 'contacts', 'is_active' => 1]);
        return view('frontend.contacts', compact('page'));
    }

    public function aboutUs()
    {
        $page = Page::firstOrNew(['slug' => 'about-us', 'is_active' => 1]);
        return view('frontend.about-us', compact('page'));
    }

    public function privacy()
    {
        $page = Page::firstOrNew(['slug' => 'privacy-policy', 'is_active' => 1]);
        return view('frontend.privacy', compact('page'));
    }
}
