<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\EmailRequest;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class ContactsController
{
    public function sendEmail(EmailRequest $request)
    {
        $attr = (object)$request->all();

        /*Mail::to('pashynskyi@gmail.com')->send(new class($attr->name, $attr->email, $attr->text, $attr->subject) extends Mailable {
            private $name;
            private $email;
            private $text;

            public function __construct($name, $email, $text, $subject = null)
            {
                $this->name = $name;
                $this->email = $email;
                $this->text = $text;
                $this->subject = $subject;
            }

            public function build()
            {
                return $this->view('frontend.emails.contacts')->with([
                    'name' => $this->name,
                    'email' => $this->email,
                    'text' => $this->text,
                    'subject' => $this->subject,
                ]);
            }
        });*/

        $headers = 'From: ' . $attr->email . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail('pashynskyi@gmail.com', $attr->subject, $attr->text, $headers);

        return redirect(route('page.contacts'))->with('message-success', __("Successfully sent!"));
    }
}