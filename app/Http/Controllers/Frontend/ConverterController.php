<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Converter;
use Illuminate\Http\Request;

class ConverterController extends Controller
{
    public function converter(Request $request)
    {
        $converterName = $request->get('name');
        $converter = Converter::firstOrNew(['name' => $converterName]);

        if(!$converter->exists) {
            return "Unknown converter name [{$converterName}]";
        }

        return view('frontend.converters.' . $converter->type, compact('converter'));
    }

    public function convertOneWay(Request $request, Converter $converter)
    {
        if(!$converter->exists) {
            return "Unknown converter";
        }

        $from = $request->get('from');
        $callback = $converter->from_callback;

        return !empty($from) && is_callable($callback) ? $from . ' = ' . $callback($from) : '';
    }

    public function convertTwoWays(Request $request, Converter $converter)
    {
        if(!$converter->exists) {
            return "Unknown converter";
        }

        $name = $request->get('name');
        $value = $request->get('value');

        $callbackName = $name . '_callback';
        $callback = $converter->$callbackName;

        return is_callable($callback) ? $callback($value) : '';
    }
}
