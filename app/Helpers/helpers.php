<?php
function highlight_menu_item($slug, $class = 'active')
{
    return $slug == \Route::currentRouteName() ? "class=$class" : '';
}

function rand_color($seed = null)
{
    if (!$seed) {
        $seed = rand();
    }

    return ('#' . substr(dechex(crc32($seed)), 0, 6));
}

function str_name($text, $sep = '_')
{
    return ucwords(str_replace($sep, ' ', $text));
}

/*
 * Example:
 * from_type = number
 * to_type = number
 * -
 * name = in_to_cm
 * from = Inches
 */
function str_array($str, $sep = '-') {
    $array = explode('-', $str);
    $res = [];

    foreach($array as $i => $itemString) {
        $itemArray = explode("\r\n", $itemString);
        foreach($itemArray as $item) {
            if(!empty($item)) {
                list($key, $value) = explode('=', $item);
                $res[$i][trim($key)] = trim($value);
            }
        }
    }

    return $res;
}