<?php

namespace App\Models\Generators;

use App\Models\Converter;

class PageGenerator
{
    private $from;
    private $to;
    private $shortFrom;
    private $shortTo;

    public function __construct($from, $to, $shortFrom, $shortTo)
    {
        $this->from = $from;
        $this->to = $to;
        $this->shortFrom = $shortFrom;
        $this->shortTo = $shortTo;
    }

    public function titleTemplate()
    {
        return "Convert $this->from to $this->to";
    }

    public function seoTitleTemplate()
    {
        return "$this->from to $this->to ($this->shortFrom to $this->shortTo) Converter";
    }

    public function seoDescriptionTemplate($group)
    {
        $toSingular = str_singular($this->to);
        $fromPlural = str_plural($this->from);
        $lowerGroup = strtolower($group);

        return "How many $fromPlural in a $toSingular? Free $fromPlural to $toSingular converter ($this->shortFrom to $this->shortTo) at myconverter.info. Use our online calculator for $lowerGroup conversions.";
    }

    public function contentTemplate($converterName, $multiplier, $isFromGreater, $exampleNum, $chartTable = null)
    {
        $toSingular = str_singular($this->to);
        $fromPlural = str_plural($this->from);
        $toPlural = str_plural($this->to);

        $oneFromEquals = Converter::convertMultiplierFrom(1, $multiplier, $isFromGreater);
        $oneToEquals = Converter::convertMultiplierTo(1, $multiplier, $isFromGreater);

        $exampleFromEquals = Converter::convertMultiplierFrom($exampleNum, $multiplier, $isFromGreater);
        $exampleAction = $isFromGreater ? '/' : '*';

        return "
<h2>How Many $fromPlural in a $toSingular?</h2>
<hr/>
<p>Enter the values below to convert $this->from ($this->shortFrom) to $this->to ($this->shortTo)</p>
<p>@conv($converterName)</p>
<h2>How to Convert $fromPlural to $toPlural ($this->shortFrom to $this->shortTo)</h2>
<p>1 $this->shortFrom = $oneFromEquals $this->shortTo&nbsp;=&nbsp;(1/$multiplier) $this->shortTo</p>
<h2>How to Convert $toPlural to $fromPlural ($this->shortTo to $this->shortFrom)</h2>
<p>1 $this->shortTo = $oneToEquals $this->shortFrom</p>
<p><strong>Example</strong></p>
<p>Convert $exampleNum $this->shortFrom to $this->shortTo:</p>
<p>$exampleNum $this->shortFrom = $exampleNum $exampleAction $multiplier $this->shortTo = $exampleFromEquals $this->shortTo</p>
<h2>" . ucfirst($this->shortFrom) . " to $this->shortTo chart</h2>
$chartTable";
    }
}