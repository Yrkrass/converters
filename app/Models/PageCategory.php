<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PageCategory
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page[] $pages
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageGroup[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageCategory whereUpdatedAt($value)
 */
class PageCategory extends Model
{
    protected $fillable = ['name', 'slug'];

    public function groups()
    {
        return $this->belongsToMany(PageGroup::class, 'category_group');
    }

    /*public function groups()
    {
        return $this->hasManyThrough(
            PageGroup::class,
            CategoryGroup::class,
            'page_category_id',
            'id',
            'id',
            'page_group_id'
        );
    }*/
}
