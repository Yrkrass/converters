<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ConverterModel
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $from
 * @property string $to
 * @property int $is_from_greater
 * @property float $multiplier
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereIsFromGreater($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereMultiplier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereUpdatedAt($value)
 * @property string $from_type
 * @property string $to_type
 * @property string $from_callback
 * @property string|null $to_callback
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereFromCallback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereFromType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereToCallback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereToType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Converter whereType($value)
 */
class Converter extends Model
{
    const TYPE_MULTIPLIER = 'multiplier';
    const TYPE_ONE_WAY = 'one_way';
    const TYPE_TWO_WAYS = 'two_ways';

    const TYPES = [self::TYPE_MULTIPLIER, self::TYPE_ONE_WAY, self::TYPE_TWO_WAYS];

    CONST INPUT_TYPE_NUMBER = 'number';
    CONST INPUT_TYPE_TEXT = 'text';

    const INPUT_TYPES = [self::INPUT_TYPE_NUMBER, self::INPUT_TYPE_TEXT];

    protected $fillable = ['name', 'from', 'to', 'from_callback', 'to_callback', 'type', 'from_type', 'to_type'];

    public function getDescription()
    {
        return "From <b>{$this->from}</b> to <b>{$this->to}</b>";
    }

    public function getMultiplierJs()
    {
        return '
            <script>
                (function() {
                    globals.converters["' . $this->name . '"] = {
                        "fromCallback": function (from) {
                            return ' . $this->from_callback . ';
                        },
                        "toCallback": function (to) {
                            return ' . $this->to_callback . ';
                        },
                        "result": "{from} ' . $this->from . ' = {to} ' . $this->to . '"
                    };
                })();
            </script>
        ';
    }

    public function getTwoWaysJs()
    {
        return '
            <script>
                (function() {
                    globals.converters["' . $this->name . '"] = {
                        "result": "{from} ' . $this->from . ' = {to} ' . $this->to . '"
                    };
                })();
            </script>
        ';
    }

    public static function getConvertersString()
    {
        $converterNames = Converter::all()->pluck('name');

        $names = [];
        foreach($converterNames as $name) {
            $names[] = "'conv({$name})'";
        }
        return implode(', ', $names);
    }

    public static function getMultiplierFromCallback($multiplier, $isFromGreater)
    {
        return ($isFromGreater ? 'from / ' : 'from * ') . $multiplier;
    }

    public static function getMultiplierToCallback($multiplier, $isFromGreater)
    {
        return ($isFromGreater ? 'to * ' : 'to / ') . $multiplier;
    }

    public static function convertMultiplierFrom($fromValue, $multiplier, $isFromGreater)
    {
        $res = $isFromGreater ? $fromValue / $multiplier : $fromValue * $multiplier;
        return round($res, self::getMultoplierLength($multiplier));
    }

    public static function convertMultiplierTo($toValue, $multiplier, $isFromGreater)
    {
        $res = $isFromGreater ? $toValue * $multiplier : $toValue * $multiplier;
        return round($res, self::getMultoplierLength($multiplier));
    }

    private static function getMultoplierLength($dig)
    {
        return strlen(preg_replace('~\D+~','', $dig));
    }
}
