<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PageCategory
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page[] $pages
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageCategory[] $categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageGroup whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageGroup whereUpdatedAt($value)
 */
class PageGroup extends Model
{
    protected $fillable = ['name', 'slug'];

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    public function categories()
    {
        return $this->belongsToMany(PageCategory::class, 'category_group');
    }

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }
}
