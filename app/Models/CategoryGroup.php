<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\CategoryGroup
 *
 * @property int $id
 * @property int $page_category_id
 * @property int $page_group_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryGroup wherePageCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryGroup wherePageGroupId($value)
 * @mixin \Eloquent
 */
class CategoryGroup extends Model
{
    protected $table = 'category_group';

    protected $fillable = ['page_category_id', 'page_group_id'];
}
