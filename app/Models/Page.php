<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string|null $seo_title
 * @property string|null $seo_description
 * @property string|null $seo_keywords
 * @property string|null $content
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @property-read \App\Models\PageCategory $category
 * @property int|null $page_group_id
 * @property-read \App\Models\PageGroup|null $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePageGroupId($value)
 */
class Page extends Model
{
    protected $fillable = ['slug', 'title', 'seo_title', 'seo_description', 'seo_keywords', 'content', 'is_active', 'page_group_id'];

    public function group()
    {
        return $this->belongsTo(PageGroup::class, 'page_group_id');
    }

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    public function showContent()
    {
        return preg_replace('/\@conv\(([\w\-]+)\)/', '<div class="converter" data-name="$1"></div>', $this->content);
    }
}
